package Singletone;

public class Q3_Singletone {

  private static Q3_Singletone singleton = null;

  private Q3_Singletone() {
    System.out.println("Instance Created");
  }

  public static Q3_Singletone getInstance() {
    if (singleton == null) {
      singleton = new Q3_Singletone();
    }
    return singleton;
  }
}

/*
 * 이 클래스가 싱글톤이 아닌 이유?
 * 
 * 내 생각...
 *  동기화 문제
 *  멀티 스레드 접근 시 getInstance에서 여러개 생성 가능. 싱크로나이즈 필요
 * 
 * 
 */
