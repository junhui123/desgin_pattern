package Singletone.Q1;

public class TicketMaker {
  private int ticket = 1000;

  private static TicketMaker ticketMaker = new TicketMaker();

  private TicketMaker() {
    System.out.println("ticket Number = " + ticket);
  }

  //synchronized가 없을 시 멀티 스레드 접근 시 값이 동일하게 나올 수 있음. 
  public synchronized int getNextTicketNumber() {
    return ticket++;
  }

  public  static TicketMaker getInstance() {
    return ticketMaker;
  }

}
