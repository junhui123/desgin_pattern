package Singletone.Q2;

public class Triple {

  private int id = 0;

  private static Triple triple1 = new Triple(0);
  private static Triple triple2 = new Triple(1);
  private static Triple triple3 = new Triple(2);

  private Triple(int num) {
    this.id = num;
  }

  public int getId() {
    return id;
  }

  public static Triple getInstance(int id) {
    if (id == 1)
      return triple1;
    else if (id == 2)
      return triple2;
    else if (id == 3)
      return triple3;
    else
      return null;
  }
}
