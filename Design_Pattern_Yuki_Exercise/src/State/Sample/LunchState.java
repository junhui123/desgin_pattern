package State.Sample;

public class LunchState implements State {

  private static LunchState singleton = new LunchState();

  private LunchState() {
  }

  public static State getInstance() {
    return singleton;
  }

  @Override
  public void doClock(Context context, int hour) {
    if (hour < 12 || hour <= 13) {
      context.changeState(DayState.getInstance());
    }
  }

  @Override
  public void doUse(Context context) {
    context.callSecurityCenter("비상 : 점심금고 사용!");
  }

  @Override
  public void doAlarm(Context context) {
    context.callSecurityCenter("비상벨(점심)");
  }

  public void doPhone(Context context) {
    context.recordLog("자동응답 점심");
  }

  public String toString() {
    return "[점심]";
  }
}
