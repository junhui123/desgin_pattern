﻿package AbstractFactory._Quiz.Q2.listFactory;
import AbstractFactory.factory.Link;
import AbstractFactory.factory.Page;
import AbstractFactory.factory.Tray;
import AbstractFactory.listfactory.*;
import AbstractFactory._Quiz.Q2.factory.*;


public class ListFactory extends Factory {
    public Link createLink(String caption, String url) {
        return new ListLink(caption, url);
    }
    public Tray createTray(String caption) {
        return new ListTray(caption);
    }
    public Page createPage(String title, String author) {
        return new ListPage(title, author);
    }
//    @Override
//    public Page createYahoo() {
//        return new ListPage("Yahoo", "http://www.yahoo.com");
//    }
}
