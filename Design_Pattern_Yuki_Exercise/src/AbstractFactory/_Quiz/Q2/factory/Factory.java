package AbstractFactory._Quiz.Q2.factory;

import AbstractFactory.factory.*;
import AbstractFactory.listfactory.ListLink;
import AbstractFactory.listfactory.ListPage;
import AbstractFactory.listfactory.ListTray;

public abstract class Factory {
  public static Factory getFactory(String classname) {
    Factory factory = null;
    try {
      factory = (Factory) Class.forName(classname).newInstance();
    } catch (ClassNotFoundException e) {
      System.err.println("클래스 " + classname + " 이 발견되지 않습니다.");
    } catch (Exception e) {
      e.printStackTrace();
    }
    return factory;
  }

  public abstract Link createLink(String caption, String url);

  public abstract Tray createTray(String caption);

  public abstract Page createPage(String title, String author);

  public Page createYahoo() {
    Link joins = new ListLink("Yahoo", "http://www.yahoo.com");

    Tray trayyahoo = new ListTray("Yahoo");
    trayyahoo.add(joins);

    Page page = new ListPage("Yahoo", "Yahoo");

    page.add(trayyahoo);

    return page;
  }

}
