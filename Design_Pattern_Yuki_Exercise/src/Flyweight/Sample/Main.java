﻿package Flyweight.Sample;

public class Main {
  public static void main(String[] args) {
    if (args.length == 0) {
      System.out.println("Usage: java Main digits");
      System.out.println("Example: java Main 1212123");
      System.exit(0);
    }
    BigString bs = new BigString(args[0]);
    bs.print();
    
    Runtime.getRuntime().gc();
    long used1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    System.out.println("사용 메모리 "+used1);
    
    System.out.println();
    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    System.out.println();
    
    
    BigString bs2 = new BigString(args[0], false);
    bs2.print();
    
    
    Runtime.getRuntime().gc();
    long used2 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    System.out.println("사용 메모리 "+used2);
    
  }
}
