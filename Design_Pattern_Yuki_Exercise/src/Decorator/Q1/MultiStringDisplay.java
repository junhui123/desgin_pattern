﻿package Decorator.Q1;

import java.awt.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class MultiStringDisplay extends Display {
  private ArrayList<String> strList = new ArrayList<>();
  
  private boolean updated = false;
  
  public MultiStringDisplay() {
    super();
  }

  public void add(String str) {
    strList.add(str);
  }

  public int getColumns() {
//    Comparator<String> byLength = (e1, e2) -> e1.length() > e2.length() ? -1 : 1;
//    Optional<String> shortest = strList.stream().sorted(byLength.reversed()).findAny();
//    return shortest.toString().length();;
    String max = Collections.max(strList, Comparator.comparing(s -> s.length()));
    return max.getBytes().length;
    
  }

  public int getRows() {
    return strList.size();
  }

  public String getRowText(int row) {
    if(updated==false) 
      updateList();
    
    return strList.get(row);
  }

  private void updateList() {
    int maxLen = getColumns();
//    for (int i = 0; i < strList.size(); i++) {
//      String temp = strList.get(i);
//      strList.set(i, temp + addBlank(maxLen - temp.getBytes().length));
//    }
    
    ArrayList<String> tempList = (ArrayList<String>) strList.stream()
        .map(f -> addBlank(f, maxLen))
        .collect(Collectors.toList());
    
    strList = tempList;    
    updated = true;
  }
  
  private String addBlank(String str, int maxLen) {
    StringBuffer tempBuf = new StringBuffer();
    int addBlank = maxLen - str.getBytes().length;
    for(int j=0; j<addBlank; j++) {
      tempBuf.append(' ');
    }
    return str+tempBuf.toString();
  }
}

//public class MultiStringDisplay extends Display {
//  private ArrayList body = new ArrayList();       // 표시 문자열
//  private int columns = 0;                      // 최대 문자수
//  public void add(String msg) {                 // 문자열 추가
//      body.add(msg);
//      updateColumn(msg);
//  }
//  public int getColumns() {                     // 문자수
//      return columns;
//  }
//  public int getRows() {                         // 행수
//      return body.size();
//  }
//  public String getRowText(int row) {            // 행의 내용
//      return (String)body.get(row);
//  }
//  private void updateColumn(String msg) {      // 문자수를 변경한다
//      if (msg.getBytes().length > columns) {
//          columns = msg.getBytes().length;
//      }
//      for (int row = 0; row < body.size(); row++) {
//          int fills = columns - ((String)body.get(row)).getBytes().length;
//          if (fills > 0) {
//              body.set(row, body.get(row) + spaces(fills));
//          }
//      }
//  }
//  private String spaces(int count) {             // 공백 작성
//      StringBuffer buf = new StringBuffer();
//      for (int i = 0; i < count; i++) {
//          buf.append(' ');
//      }
//      return buf.toString();
//  }
//}