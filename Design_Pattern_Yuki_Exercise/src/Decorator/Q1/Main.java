﻿package Decorator.Q1;

public class Main {
  public static void main(String[] args) {
//Quiz 1)
//    Display b1 = new StringDisplay("Hello, world.");
//    b1.show();
//    Display b2 = new UpDownBorder(b1, '-');
//    b2.show();
//    Display b3 = new SideBorder(b2, '*');
//    b3.show();
//    Display b4 = new FullBorder(new UpDownBorder(new SideBorder(new UpDownBorder(new SideBorder(new StringDisplay("안녕하세요."), '*'), '='), '|'), '/'));
//    b4.show();
    
//Quiz 2)
    MultiStringDisplay md = new MultiStringDisplay();
    md.add("좋은 아침입니다.");
    md.add("안녕하세요.");
    md.add("안녕히 주무세요. 내일 만나요.");
    md.show();
    
    Display d1 = new SideBorder(md, '#');
    d1.show();

    Display d2 = new FullBorder(md);
    d2.show();
  }
}
