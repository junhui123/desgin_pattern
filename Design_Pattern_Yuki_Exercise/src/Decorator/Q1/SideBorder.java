﻿package Decorator.Q1;

public class SideBorder extends Border {
  private char borderChar;

  public SideBorder(Display display, char ch) {
    super(display);
    this.borderChar = ch;
  }

  public int getColumns() {
    int dispColumns = display.getColumns();
    return 1 + dispColumns + 1;
  }

  public int getRows() {
    return display.getRows();
  }

  public String getRowText(int row) {
    String rowText = display.getRowText(row);
    String str = borderChar + rowText + borderChar;
    return str;
  }
}
