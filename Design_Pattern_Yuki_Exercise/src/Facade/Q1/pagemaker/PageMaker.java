﻿package Facade.Q1.pagemaker;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class PageMaker {
  private PageMaker() {
  }

  public static void makeWelcomePage(String mailaddr, String filename) {
    try {
      Properties mailprop = Database.getProperties("maildata");
      String username = mailprop.getProperty(mailaddr);
      HtmlWriter writer = new HtmlWriter(new FileWriter(filename));
      writer.title("Welcome to " + username + "'s page!");
      writer.paragraph(username + "의 페이지에 오신 걸 환영합니다.");
      writer.paragraph("메일을 기다리고 있습니다.");
      writer.mailto(mailaddr, username);
      writer.close();
      System.out.println(filename + " is created for " + mailaddr + " (" + username + ")");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  public static void makeLinkPage(String linkPage) throws IOException {
    HtmlWriter writer = new HtmlWriter(new FileWriter(linkPage));
    writer.title("Link Page");
    writer.paragraph("");
    
    
    Properties mailprop = Database.getProperties("C:\\eclipse_workspace\\Design_Pattern_Yuki_Exercise\\src\\Facade\\Q1\\maildata");
    for (String key : mailprop.stringPropertyNames() )  {
      String href = key;
      String caption  = mailprop.getProperty(key);
      writer.link(href, caption);
    }
    writer.close();
  }
}
