﻿package Facade.Q1;

import java.io.IOException;

import Facade.Q1.pagemaker.PageMaker;

public class Main {
    public static void main(String[] args) {
//        PageMaker.makeWelcomePage("youngjin@youngjin.com", "welcome.html");
        
        try {
          PageMaker.makeLinkPage("linkpage.html");
        } catch (IOException e) {
          e.printStackTrace();
        }
    }
}
