package ProtoType.Q1_1;

import ProtoType.Q1_1.framework.Product;

public class ProductImpl implements Product {

  @Override
  public Product createClone() {
    Product p = null;
    try {
      p = (Product) clone();
    } catch (CloneNotSupportedException e) {
      e.printStackTrace();
    }
    return p;
  }

  @Override
  public void use(String s) {
    // TODO Auto-generated method stub
    
  }
}
