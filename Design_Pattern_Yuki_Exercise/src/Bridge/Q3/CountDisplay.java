﻿package Bridge.Q3;

public class CountDisplay extends Display {
  public CountDisplay(DisplayImpl impl) {
    super(impl);
  }

  public void multiDisplay(int times) { // times회 반복해서 표시하기
    for (int i = 0; i <= times; i++) {
      open();
      if (i != 0) {
        for (int z = 0; z < i; z++)
          print();
      }
      close();
    }
  }
}
