﻿package Bridge.Q3;

public class StringDisplayImpl extends DisplayImpl {
  private String string;

  private StringBuffer sBuf;

  public StringDisplayImpl(String string) {
    this.string = string;
  }

  public void rawOpen() {
    sBuf = new StringBuffer();
    sBuf.append("<");
  }

  public void rawPrint() {
    sBuf.append(string);
  }

  public void rawClose() {
    sBuf.append(">");
    System.out.println(sBuf.toString());
  }

}
