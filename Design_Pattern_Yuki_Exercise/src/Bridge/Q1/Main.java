﻿package Bridge.Q1;

import java.util.concurrent.ThreadLocalRandom;


public class Main {
    public static void main(String[] args) {
        RandomDisplay r1 = new RandomDisplay(new StringDisplayImpl("Random"));
        int times = ThreadLocalRandom.current().nextInt(0, 9 + 1);
        System.out.println("times = " + times);
        System.out.println("========================================");
        r1.randomDisplay(times);
        
        
    }
}
