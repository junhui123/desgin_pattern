﻿package Bridge.Q1;

public class RandomDisplay extends Display {
  public RandomDisplay(DisplayImpl impl) {
    super(impl);
  }

  public void randomDisplay(int times) { // times회 반복해서 표시하기
    if (times > 0) {
      open();
      for (int i = 0; i < times; i++) {
        print();
      }
      close();
    } else {
      System.out.println("ranndom times count less than 0");
    }
  }
}
