﻿package Bridge.Q2;

public class Main {
    public static void main(String[] args) {
        TextDisplayImpl t1 = new TextDisplayImpl("C:\\eclipse_workspace\\Design_Pattern_Yuki_Exercise\\src\\Bridge\\Q2\\SampleText.txt");
        Display disp = new Display(t1);
        disp.display();
    }
}
