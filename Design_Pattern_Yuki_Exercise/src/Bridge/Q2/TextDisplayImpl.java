﻿package Bridge.Q2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class TextDisplayImpl extends DisplayImpl {
  private String filePath;
  private Stream<String> stream;

  public TextDisplayImpl(String filePath) {
    this.filePath = filePath;
  }

  public void rawOpen() {
    try {
      stream = Files.lines(Paths.get(filePath));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void rawPrint() {
    stream.forEach(System.out::println);
  }

  public void rawClose() {
    stream.close();
  }

}
 