package Memento.Sample.game;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Util {

  private static final long serialVersionUID = -2127002381707416117L;
  public static String FILE_NAME = "game.dat";

  public static void save(Memento memento) {
    try (FileOutputStream fos = new FileOutputStream(FILE_NAME); ObjectOutputStream oos = new ObjectOutputStream(fos);) {
      oos.writeObject(memento);
      System.out.println("memento save");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static Memento restore() {
    Memento memento = null;
    try (FileInputStream fis = new FileInputStream(FILE_NAME); ObjectInputStream ois = new ObjectInputStream(fis);) {
      memento = (Memento) ois.readObject();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return memento;
  }

  public static boolean isSavedDataExists() {
    File file = new File(FILE_NAME);
    boolean isExists = file.exists();
    if (isExists) {
      return true;
    } else {
      return false;
    }
  }
}
