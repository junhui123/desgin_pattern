﻿package Memento.Sample.game;

import java.io.Serializable;
import java.util.*;

public class Memento implements Serializable {
  int number;
  int money; // 소지금
  ArrayList fruits; // 과일

  public int getMoney() { // 소지금을 얻는다(narrow interface)
    return money;
  }

  int getNumber() {
    return number;
  }

  Memento(int money) { // 생성자(wide interface)
    this.money = money;
    this.fruits = new ArrayList();
  }

  Memento(int money, int number) { // 생성자(wide interface)
    this.money = money;
    this.number = number;
    this.fruits = new ArrayList();
  }

  void addFruit(String fruit) { // 과일을 추가한다(wide interface)
    fruits.add(fruit);
  }

  List getFruits() { // 과일을 얻는다(wide interface)
    return (List) fruits.clone();
  }
}
