﻿package Composite;

public abstract class Entry {
  public Entry parent;

  public abstract String getName();

  public abstract int getSize();

  public Entry add(Entry entry) throws FileTreatmentException {
    throw new FileTreatmentException();
  }

  public void printList() {
    printList("");
  }

  protected abstract void printList(String prefix);

  public String toString() {
    return getName() + " (" + getSize() + ")";
  }

  // Q 11-2
  public String getFullPath() {
    StringBuffer sbuf = new StringBuffer();
    Entry entry = this;
    
    while(entry != null) {
      sbuf.insert(0,  "/" + entry.getName());
      entry = entry.parent;
    }
    
    return sbuf.toString();
    
  }
}
