﻿package Composite;

public class Main {
    public static void main(String[] args) {
        try {
//            System.out.println("Making root entries...");
//            Directory rootdir = new Directory("root");
//            Directory bindir = new Directory("bin");
//            Directory tmpdir = new Directory("tmp");
//            Directory usrdir = new Directory("usr");
//            rootdir.add(bindir);
//            rootdir.add(tmpdir);
//            rootdir.add(usrdir);
//            bindir.add(new File("vi", 10000));
//            bindir.add(new File("latex", 20000));
//            rootdir.printList();
//
//            System.out.println("");
//            System.out.println("Making user entries...");
//            Directory Kim = new Directory("Kim");
//            Directory Lee = new Directory("Lee");
//            Directory Park = new Directory("Park");
//            usrdir.add(Kim);
//            usrdir.add(Lee);
//            usrdir.add(Park);
//            Kim.add(new File("diary.html", 100));
//            Kim.add(new File("Composite.java", 200));
//            Lee.add(new File("memo.tex", 300));
//            Park.add(new File("game.doc", 400));
//            Park.add(new File("junk.mail", 500));
//            rootdir.printList();
            
          Directory rootdir = new Directory("root");

          Directory usrdir = new Directory("usr");
          rootdir.add(usrdir);

          Directory youngjin = new Directory("youngjin");
          usrdir.add(youngjin);

          File file = new File("Composite.java", 100);
          youngjin.add(file);
          rootdir.printList();

          System.out.println("");
          System.out.println("file = " + file.getFullPath());     
          System.out.println("youngjin = " + youngjin.getFullPath());     
            
            
        } catch (FileTreatmentException e) {
            e.printStackTrace();
        }
    }
}
