package Observer.Sample;

public class IncrementalNumberGenerator extends NumberGenerator {

  private int start = 0;
  private int end = 0;
  private int increment;
  
  public IncrementalNumberGenerator(int start, int end, int increment) {
    super();
    this.start = start;
    this.end = end;
    this.increment = increment;
  }

  private int number;                        // 현재의 수
  
  @Override
  public int getNumber() {                   // 수를 취득한다
      return number;
  }
  
  @Override
  public void execute() {
      for (int i = start; i < end; i=i+increment) {
          number = i;
          notifyObservers();           
      }
  }
}
