﻿package Observer.Sample;

public class Main {
    public static void main(String[] args) {
//        Q1
        NumberGenerator generator = new IncrementalNumberGenerator(10, 50, 5); 
//        NumberGenerator generator = new RandomNumberGenerator();
        Observer observer1 = new DigitObserver();
        Observer observer2 = new GraphObserver();
        Observer observer3 = new ConcreteObserver();
        generator.addObserver(observer1);
        generator.addObserver(observer2);
        generator.addObserver(observer3);
        generator.execute();
    }
}
