package Visitor.Q1;

import java.util.ArrayList;
import java.util.Iterator;

public class SizeVisitor extends Visitor {

//  private ArrayList dir = new ArrayList();

  @Override
  public void visit(File file) {
    System.out.print(file.getSize());
  }

  @Override
  public void visit(Directory directory) {
    int size = 0;
    Iterator it = directory.iterator();
    while (it.hasNext()) {
      Entry entry = (Entry) it.next();
      size += entry.getSize();
      directory.size = size;
    }
  }
}
