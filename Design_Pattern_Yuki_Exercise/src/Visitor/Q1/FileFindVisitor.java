package Visitor.Q1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FileFindVisitor extends Visitor {

  private String fileExtension = "";
  private List<File> foundList = new ArrayList<>();
  
  public FileFindVisitor(String fileExtension) {
    super();
    this.fileExtension = fileExtension;
  }

  @Override
  public void visit(File file) {
    String fileName = file.getName();
    if(fileName.contains(fileExtension)) {
      foundList.add(file);
    }
  }

  @Override
  public void visit(Directory directory) {
    Iterator it = directory.iterator();
    while (it.hasNext()) {
        Entry entry = (Entry)it.next();
        entry.accept(this);
    }
  }
  
  public Iterator getFoundFiles() {
    return foundList.iterator();
  }
}
