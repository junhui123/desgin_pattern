package Visitor.Q1;

import java.util.ArrayList;
import java.util.Iterator;

public class ElementArrayList extends ArrayList<Entry> implements Element {

  @Override
  public boolean add(Entry e) {
    return super.add(e);
  }

  public void accept(Visitor v) {
    Iterator it = this.iterator();
    while (it.hasNext()) {
      Entry entry = (Entry) it.next();

      if (entry instanceof Directory) {
        v.visit((Directory) entry);

      } else {
        v.visit((File) entry);
      }
    }
  }
}
