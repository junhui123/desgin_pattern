package Builder.Q3;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesBuilder extends Properties implements Builder {

  private String title = "";
  private String firstStr = "";
  private FileOutputStream fOut;

  @Override
  public void makeTitle(String title) {
    this.title = title;
    try {
      fOut = new FileOutputStream(title+".properties");
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  @Override
  public void makeString(String str) {
    this.firstStr += str;
  }

  @Override
  public void makeItems(String[] items) {
    
    for (int i = 0; i < items.length; i++) {
      String key = String.valueOf(Math.random());
      String value = items[i];

      setProperty(key, value);
    }
  }

  @Override
  public void close() {
    try {
      store(fOut, firstStr);
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public String getResult() {
    return this.title;
  }

}
