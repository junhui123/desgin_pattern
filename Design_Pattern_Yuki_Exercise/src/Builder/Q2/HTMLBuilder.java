﻿package Builder.Q2;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class HTMLBuilder extends Builder {
  private String filename;
  private PrintWriter writer;

  public void makeTitle(String paramString) {
    this.filename = paramString + ".html";
    try {
      this.writer = new PrintWriter(new FileWriter(this.filename));
    } catch (IOException localIOException) {
      localIOException.printStackTrace();
    }
    this.writer.println("<html><head><title>" + paramString + "</title></head><body>");
    this.writer.println("<h1>" + paramString + "</h1>");
  }

  public void makeString(String paramString) {
    this.writer.println("<p>" + paramString + "</p>");
  }

  public void makeItems(String[] paramArrayOfString) {
    this.writer.println("<ul>");
    for (int i = 0; i < paramArrayOfString.length; ++i) {
      this.writer.println("<li>" + paramArrayOfString[i] + "</li>");
    }
    this.writer.println("</ul>");
  }

  public void close() {
    this.writer.println("</body></html>");
    this.writer.close();
  }

  public String getResult() {
    return this.filename;
  }
}