package Startegy.Q4;

public class BubbleSorter implements Sorter {

  @SuppressWarnings({ "unchecked", "rawtypes" })
  @Override
  public void sort(Comparable[] data) {
    for (int i = 0; i < data.length; i++) {
      int min = 0;
      for (int j = i; j < data.length; j++) {
        if (data[i].compareTo(data[j]) > 0) {
          min = j;
          Comparable passingplace = data[min];
          data[min] = data[i];
          data[i] = passingplace;
        }
      }
    }
  }
}
