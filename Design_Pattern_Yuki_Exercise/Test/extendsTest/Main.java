package extendsTest;

public class Main {
  public static void main(String[] args) {
    Parent obj = new Child();
    obj.parentMethod();
    
    Parent obj2 = new Child();
    ((Child)obj2).childMethod();
    ((Child)obj2).parentMethod();
  }
}
