package extendsTest;

public class Child extends Parent {
  public void childMethod() {
    System.out.println("child method");
  }

  @Override
  public void parentMethod() {
    System.out.println("override parent method child");
  }
  
  
  
}
