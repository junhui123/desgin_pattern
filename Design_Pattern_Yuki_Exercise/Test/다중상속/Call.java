package 다중상속;

import State.Sample.DayState;

public class Call implements PlaneCar {
  public static void main(String[] arags) {
    new Call().call();
  }

  public void call() {
    fly();
    drive();
    carry();
  }
}
