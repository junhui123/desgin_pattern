package 다중상속;

public interface Plane {
  default void fly() {
    System.out.println("I can fly.");
  }

  default void carry() {
    System.out.println("plane carry");
  }
}
