package 다중상속;

public interface PlaneCar extends Plane, Car{
  @Override default void carry(){
      Plane.super.carry();
      Car.super.carry();
  }
}


