package 행위패턴.Observer;

import java.util.Observable;
import java.util.Observer;

public class Employee implements Observer {
	private String desc;

	public Employee(String desc) {
		this.desc = desc;
	}

	//update를 호출해준 Observable(Watcher 클래스)
	//하나의 Observer는 여러개의 Observable에 등록 가능
	//Employee는 사장이 오는지 감시하는 Observable이외
	//퇴근 시간 알려주는 Observable로 부터 통보 받을 수 있음
	
	//args = 구체적인 정보를 받을 수 있습니다
	public void update(Observable o, Object arg) {
		if (o instanceof Watcher) {
			System.out.println(desc + "이 일하는 척");
		}
	}

	public String getDesc() {
		return desc;
	}
}