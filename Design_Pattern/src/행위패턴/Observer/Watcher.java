package 행위패턴.Observer;

import java.util.Observable;

/**
 * 어떤 클래스에 변화가 일어났을 때, 다른 클래스에 통보해 주는 패턴.
 * 통보를 하는 "어떤 클래스"가 Observable 이고, 통보를 받는 "다른 클래스"는 Observer입니다.
 * Observable은 여러개의 Observer를 가질 수 있습니다.
 * Observable은 통보만 해줄 뿐 Observer에게 상세하게 통보에 대해서 어떻게 처리하라는 지시를 하지 않음.
 */

//변화를 통보하는 Observable
public class Watcher extends Observable {
	public void action(String string) {
		System.out.println("=======" + string + "========");
		//setChanged = 변화가 일어났다는 것을 알림
		//원하는 변화에 대해서 확인 후 Observer에게 알리는 처리가 필요
		setChanged(); 
		
		// Observer들에게 전부 알림. Observer들은 각각 자기가 가진 update() 메쏘드가 호출
		// setChanged 호출되지 않고 notifyObservers 호출 시 작동하지 않음
		notifyObservers(string); 
	}
}