package 행위패턴.visitor;

public interface Visitor {
	void visit(Acceptor acceptor);
}
