package 행위패턴.visitor;

public interface Acceptor {
	void accept(Visitor visitor);
}
