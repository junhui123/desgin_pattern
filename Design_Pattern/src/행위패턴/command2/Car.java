package 행위패턴.command2;

public class Car {
	private StartCar startCarCommand = null;

	public Car(StartCar startCarCommand) {
		this.startCarCommand = startCarCommand;
	}

	public void startCar() {
		if (startCarCommand != null) {
			startCarCommand.execute();
		}
	}

	public void doParking() {
		if (startCarCommand != null) {
			startCarCommand.cancel();
		}
	}
}
