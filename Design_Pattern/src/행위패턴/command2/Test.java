package 행위패턴.command2;

public class Test {
	public static void main(String[] args) {
		StartCar  startCarCommand = new StartCar();
		Car myCar = new Car(startCarCommand); 
		myCar.startCar();
		myCar.doParking();
	}
}
