package 행위패턴.command2;

public class StartCar {
	public void execute() {
		System.out.println("StartCar.execute");
	}

	public void cancel() {
		System.out.println("StartCar.cancel");
	}
}
