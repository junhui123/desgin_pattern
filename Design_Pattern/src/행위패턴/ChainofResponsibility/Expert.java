package 행위패턴.ChainofResponsibility;


/*
 * Chain of Responsiblity 패턴에서는 문제 해결사들이 한줄로 쫙 서있다가 문제가 들어오면, 자기가 해결할 수 있으면 해결하고, 안 되면 다음 해결사에게 문제를 넘겨버립니다.
 * 처리 위임을 하는 객체의 순서가 전체 수행 속도에 영향 줄 수 있음.
 * 일반적인 처리 가능 경우 부터 파악 후 앞으로 순서 지정
 */

public abstract class Expert {
	//자기 자신을 클래스 변수로 가짐(Decorator와 비슷 차이는 null이 가능)
	//다음 위임할 객체가 없으면 null
	private Expert next; 
	protected String expertName;

	//해결 가능한 경우 support에서 처리 못하는 경우 위임
	public final void support(Problem p) {
		//solve = 각각 개별 클래스 객체에서 해결이 가능한지 확인
		//템플릿메소드 적용 (구체적 구현은 하위 클래스에서 담당)
		if (solve(p)) {
			System.out.println(expertName + "이(가) " + p.getProblemName() + "을(를) 해결해 버렸네.");
		} else {
			if (next != null) {
				next.support(p);
			} else {
				System.out.println(p.getProblemName() + "은(는) 해결할 넘이 없다.");
			}
		}
	}

	public Expert setNext(Expert next) {
		this.next = next;
		return next;
	}

	//템플릿메소드 적용 (구체적 구현은 하위 클래스에서 담당)
	protected abstract boolean solve(Problem p);
}
