package 행위패턴.ChainofResponsibility;

public class Fighter extends Expert {
	public Fighter() {
		this.expertName = "격투가";
	}

	@Override
	protected boolean solve(Problem p) {
		return p.getProblemName().contains("깡패");
	}
}