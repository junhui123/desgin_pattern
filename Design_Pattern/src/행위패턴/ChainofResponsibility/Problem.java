package 행위패턴.ChainofResponsibility;

public class Problem {
	private String problemName;

	public Problem(String name) {
		this.problemName = name;
	}

	public String getProblemName() {
		return problemName;
	}
}