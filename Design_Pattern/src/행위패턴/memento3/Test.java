package 행위패턴.memento3;

import java.util.Stack;

import 행위패턴.memento3.abc.Memento;
import 행위패턴.memento3.abc.Originator;

public class Test {

	public static void main(String[] args) {

		Stack<Memento> mementos = new Stack<>();

		Originator originator = new Originator();

		originator.setState("state 1");
		mementos.push(originator.createMemento());
		// 외부에서 제어하지 못하도록 proteced 선언을 사용했므로 아래 부분 처럼 사용 불가
		// Memento memento = new Memento();//X
		// memento.getState();//X
		originator.setState("state 2");
		mementos.push(originator.createMemento());
		originator.setState("state 3");
		mementos.push(originator.createMemento());
		originator.setState("state Final");
		mementos.push(originator.createMemento());

		originator.restoreMement(mementos.pop());
		System.out.println(originator.getState());// state Final
		originator.restoreMement(mementos.pop());
		System.out.println(originator.getState());// state 3
		originator.restoreMement(mementos.pop());
		System.out.println(originator.getState());// state 2
		originator.restoreMement(mementos.pop());
		System.out.println(originator.getState());// state 1

	}
}
