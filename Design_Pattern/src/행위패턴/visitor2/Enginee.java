package 행위패턴.visitor2;

//데이터 클래스들 (Visitor에게 자신을 단지 던져줌으로써 끝)
class Enginee implements CarElement {
	public void accept(CarElementVisitor visitor) {
		visitor.visit(this);
	}
}
