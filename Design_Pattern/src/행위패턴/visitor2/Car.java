package 행위패턴.visitor2;

//데이터 클래스들 (Visitor에게 자신을 단지 던져줌으로써 끝)
public class Car implements CarElement {
	CarElement[] elements;

	public CarElement[] getElements() {
		return elements.clone(); // Return a copy of the array of references.
	}

	@Override
	public void accept(CarElementVisitor visitor) {
		for (CarElement element : this.getElements()) {
			element.accept(visitor);
		}
		visitor.visit(this);
	}

	public Car() {
		this.elements = new CarElement[] { new Wheel("front left"), new Wheel("front right"), new Wheel("back left"), new Wheel("back right"), new Body(), new Enginee() };
	}
}
