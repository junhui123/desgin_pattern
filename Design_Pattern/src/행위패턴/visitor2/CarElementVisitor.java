package 행위패턴.visitor2;

//Visitor 인터페이스
public interface CarElementVisitor {
	void visit(Wheel wheel);
	void visit(Enginee enginee);
	void visit(Body body);
	void visit(Car car);
}
