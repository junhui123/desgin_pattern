package 행위패턴.visitor2;

//클라이언트
public class Test {
	public static void main(String[] args) {
		Car car = new Car();
		car.accept(new CarElementPrintVisitor()); // 알고리즘 적용
		car.accept(new CarElementDoVisitor()); // //알고리즘 적용
	}
}


/**
방문자(Visitor)는 자기가 방문하게될 요소(Ac)에 자기를 등록하도록 Acceptor(방문자 v); 가 요구되고 이를 인터페이스로 구현된다. 
 
이 인터페이스를 통해 방문자의 .Visit( 요소 )를 호출하면서 구조(자료리스트)를 순회 하게 된다. 

단순하게 생각하자!! 

방문자는   방문하게될 대상에 자기가 들어갈 곳을(Acceptor) 요구하고 이를 통해 자기가 들어가서

방문(Visit())을 하게 되며  대상요소에 대한 처리는 Visit() 내에서 처리한다!!..

출처: http://aseuka.tistory.com/entry/비지터-패턴 [퇴근5분전]
**/