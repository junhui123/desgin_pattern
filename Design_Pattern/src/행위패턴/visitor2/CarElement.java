package 행위패턴.visitor2;

//데이터 인터페이스 (accept 함수 제공)
public interface CarElement {
	void accept(CarElementVisitor visitor);

}
