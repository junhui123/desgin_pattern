package 행위패턴.visitor2;

//데이터 클래스들 (Visitor에게 자신을 단지 던져줌으로써 끝)
class Wheel implements CarElement {
	private String name;

	public Wheel(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void accept(CarElementVisitor visitor) {
		visitor.visit(this);
	}
}