package 행위패턴.ChainofResponsibility2;

public class Test {
	public static void main(String[] args) {
		Support alice = new NoSupport("Alice");
		Support bob = new LimitSupport("Bob", 100);
		Support diana = new LimitSupport("Diana", 200);
		Support fred = new LimitSupport("Fred", 300);

		// 연쇄의 형성
		alice.setNext(bob).setNext(diana).setNext(diana).setNext(fred);

		// 다양한 트러블 발생
		for (int i = 0; i < 500; i += 33) {
			alice.support(new Trouble(i));
		}
	}

}
