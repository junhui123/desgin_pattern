package 행위패턴.ChainofResponsibility2;

//트러블을 나타내면 트러블 번호를 갖는다. 나중에 처리하는 클래스들은 이 번호를 가지고 자신이
// 처리 할 수 있는지의 여부를 결정한다.
public class Trouble {
	private int number; // 트러블 번호

	public Trouble(int number) { // 트러블의 생성
		this.number = number;
	}

	public int getNumber() { // 트러블 번호를 얻는다.
		return number;
	}

	public String toString() { // 트러블의 문자열 표현
		return "[Trouble " + number + "]";
	}
}
