package 행위패턴.Template_Method2;

public abstract class Sandwich {
  
  //cook = 템플릿에 해당
  public void Cook() {
    bread();
    System.out.println("양상추");
    topping();
    System.out.println("토마토");
    bread();
  }
  
  //세부적 기능 = 하위 클래스에서 구현
  public abstract void bread();
  public abstract void topping();
}
