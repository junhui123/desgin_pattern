package 행위패턴.Template_Method2;


//http://itcrowd2016.tistory.com/22
public class Test {
  public static void main(String[] args) {

    Sandwich ham = new Ham();
    ham.Cook();
    System.out.println("=================");
    Sandwich tuna = new Tuna();
    tuna.Cook();

  }
}
