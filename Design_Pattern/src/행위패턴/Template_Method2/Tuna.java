package 행위패턴.Template_Method2;

public class Tuna extends Sandwich{

  @Override
  public void bread() {
    System.out.println("white bread 한 장");
  }

  @Override
  public void topping() {
    System.out.println("참치");
  }
  
  

}
