package 행위패턴.Template_Method2;

public class Ham extends Sandwich {

  @Override
  public void bread() {
    System.out.println("brown bread 한장");
  }

  @Override
  public void topping() {
    System.out.println("햄");
  }
}
