package 행위패턴.Mediator4;

public class Test {
  public static void main(String[] args) {
    IMediator mediator = new ConcreateMediator();

    ColleagueA talkColleague = new ColleagueA(mediator);
    ColleagueB fightColleague = new ColleagueB(mediator);

    talkColleague.doSomething();
    fightColleague.doSomething();

  }
}
