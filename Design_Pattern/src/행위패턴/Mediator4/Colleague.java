package 행위패턴.Mediator4;

public abstract class Colleague {

  IMediator mediator;

  public abstract void doSomething();
}
