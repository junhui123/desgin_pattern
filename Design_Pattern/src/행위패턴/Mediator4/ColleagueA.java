package 행위패턴.Mediator4;

public class ColleagueA extends Colleague {
  
  public ColleagueA(IMediator mediator) {
    super();
    this.mediator = mediator;
  }

  @Override
  public void doSomething() {
    this.mediator.talk();
    this.mediator.registerA(this);
  }

}
