package 행위패턴.Mediator4;

public class ColleagueB extends Colleague {


  public ColleagueB(IMediator mediator) {
    super();
    this.mediator = mediator;
  }

  @Override
  public void doSomething() {
    this.mediator.fight();
    this.mediator.registerB(this);
  }

}
