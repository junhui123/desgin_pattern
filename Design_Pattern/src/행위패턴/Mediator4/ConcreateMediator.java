package 행위패턴.Mediator4;

public class ConcreateMediator implements IMediator {

  ColleagueA talk;
  ColleagueB fight;

  @Override
  public void fight() {
    System.out.println("Mediato fight");
  }

  @Override
  public void talk() {
    System.out.println("Mediato talk");

  }

  @Override
  public void registerA(ColleagueA a) {
    talk = a;
  }

  @Override
  public void registerB(ColleagueB b) {
    fight = b;
  }

}
