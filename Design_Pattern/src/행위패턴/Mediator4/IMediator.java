package 행위패턴.Mediator4;

public interface IMediator {
  public void fight();
  public void talk();
  public void registerA(ColleagueA a);
  public void registerB(ColleagueB b);
}
