package 행위패턴.Strategy3.delegation;

public abstract class DisplayImpl {
	public abstract void rawOpen();
	public abstract void rawClose();
	public abstract void rawPrint();
}
