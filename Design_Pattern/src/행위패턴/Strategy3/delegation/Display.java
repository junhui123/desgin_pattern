package 행위패턴.Strategy3.delegation;

//Display 클래스의 impl 필드에는 구현되는 인스턴스가 저정되어 있어서
//open, print, close를 호출 시 DisplayImpl impl의 rawOpen, rawCloase, rawPrint를 호출
//이를 위임이라고 한다. Display 클래스 인스턴스 생성 단계에서 인수로 전달되어온 DisplayImpl과 느슨한 연결
public class Display {
	private DisplayImpl impl;

	public Display(DisplayImpl impl) {
		this.impl = impl;
	}

	public void open() {
		impl.rawOpen();
	}

	public void print() {
		impl.rawPrint();
	}

	public void close() {
		impl.rawClose();
	}

	public final void display() {
		open();
		print();
		close();
	}
}
