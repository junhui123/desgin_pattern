package 행위패턴.Strategy3;

public class Test {
	public static void main(String[] args) {
		// 칼 휘두르는 캐릭터 생성!
		Character character1 = new Character(new Knife());
		character1.fight();

		// 도끼를 휘두르는 캐릭터 생성
		Character character2 = new Character(new Axe());
		character2.fight();

		// 도끼에서 활로 무기 변경
		character2.setWeapon(new BowAndArrow());
		character2.fight();
	}
}
