package 행위패턴.Strategy;

/*
 * Template Method 패턴과 유사
 * Template Method는 하위 클래스에서 변경되는 부분을 처리 
 * Strategy는 인터페이스로 분리하여 인터페이스 구현체에서 처리
 * 어떤 작업을 수행하기 위해서 능동적으로 알고리즘을 변경 할 수 있는 패턴


 */
public interface Seller {
	public void sell();
}
