package 행위패턴.Strategy;

/*
 * Seller 클래스의 Sell() 호출하지 않음. Mart의 order()를 호출
 * Seller는 외부로 공개되지 않음
 * Mart 클래스가 외부로 공개되는 Context
 * 달리지는 부분 찾아내고 달라지지 않는 부분으로 부터 분리 
 * 상속보다 구성을 활용 ( Composition )
 */
public class Test {
	public static void main(String[] args) {
		Seller cupSeller = new CupSeller();
		Seller phoneSeller = new PhoneSeller();
		Mart mart1 = new Mart(cupSeller);
		mart1.order();
		Mart mart2 = new Mart(phoneSeller);
		mart2.order();
	}
}