package 행위패턴.ChainofResponsibility4;


public class Attack {

	private int amount;

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getAmount() {
		return amount;
	}

}