package 행위패턴.ChainofResponsibility4;

public class Application {
	public static void main(String[] args) {
		Attack attack = new Attack();
		attack.setAmount(100);

		Armor armorA = new Armor(10);
		Armor armorB = new Armor(15);

		armorA.setNextArmor(armorB);
		armorA.depense(attack);
		System.out.println(attack.getAmount());

		attack.setAmount(100);
		System.out.println(attack.getAmount());
		Defense defense = new Defense() {
			@Override
			public void depense(Attack attack) {
				int amount = attack.getAmount();
				attack.setAmount(amount -= 50);
			}
		};
		armorB.setNextArmor(defense);
		armorA.depense(attack);
		System.out.println(attack.getAmount());
	}
}
