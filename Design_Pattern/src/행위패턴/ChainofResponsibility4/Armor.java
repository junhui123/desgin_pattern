package 행위패턴.ChainofResponsibility4;
public class Armor implements Defense {

	private Defense nextDefense;
	private int def;

	public void setDef(int def) {
		this.def = def;
	}

	public Armor() {
		// TODO Auto-generated constructor stub
	}

	public Armor(int def) {
		this.def = def;

	}

	public void setNextArmor(Defense nextDefense) {
		this.nextDefense = nextDefense;
	}

	@Override
	public void depense(Attack attack) {

		proccess(attack);
		if (nextDefense != null)
			nextDefense.depense(attack);
	}

	private void proccess(Attack attack) {
		int amount = attack.getAmount();
		amount -= def;
		attack.setAmount(amount);
	}

}
