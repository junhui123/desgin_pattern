package 행위패턴.state2;

public class TVStopState implements State {

	@Override
	public void doAction() {
		System.out.println("TV OFF");
	} 
}
