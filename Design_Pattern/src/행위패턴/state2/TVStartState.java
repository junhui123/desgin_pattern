package 행위패턴.state2;

public class TVStartState implements State {

	@Override
	public void doAction() {
		System.out.println("TV ON");
	} 
}
