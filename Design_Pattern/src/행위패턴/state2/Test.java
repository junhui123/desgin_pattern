package 행위패턴.state2;

public class Test {

	public static void main(String[] args) {
		TVContext context = new TVContext();

		State tvStartState = new TVStartState();
		State tvStopState = new TVStopState();

		context.setTvState(tvStartState);
		context.doAction();

		context.setTvState(tvStopState);
		context.doAction();
	}
}
