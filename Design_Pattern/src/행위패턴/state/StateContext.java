package 행위패턴.state;

public class StateContext {

	
	private State currentState;
	
	public StateContext() {
		this.currentState = new Poor();
	}

	public void changeState(State state) {
		this.currentState = state;
	}
	
	public void saySomething() {
		this.currentState.saySomething(this);
	}
	

}
