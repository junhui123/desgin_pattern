package 행위패턴.state;

public class Test {
	public static void main(String[] args) {
		StateContext sc = new StateContext();
		
		sc.saySomething();
		sc.saySomething();
		sc.saySomething();
		sc.saySomething();
		
	}
}
