package 행위패턴.state;

public class Rich implements State {

	@Override
	public void saySomething(StateContext sc) {
		 System.out.println("Rich");
		 sc.changeState(new Poor());
	}
}
