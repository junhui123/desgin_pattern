package 행위패턴.state;

public class Poor implements State{
	
	@Override
	public void saySomething(StateContext sc) {
		System.out.println("Poor");
		sc.changeState(new Rich());
	}

}
