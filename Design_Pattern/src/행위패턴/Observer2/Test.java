package 행위패턴.Observer2;

public class Test {
	public static void main(String[] args) {
		NewsMachine newsMachine = new NewsMachine();
		AnnualSubscriber as = new AnnualSubscriber(newsMachine);
		EventSubscriber es = new EventSubscriber(newsMachine);

		newsMachine.setNewsInfo("1", "2");
		es.withdraw();
		newsMachine.setNewsInfo("a", "b");
	}
}
