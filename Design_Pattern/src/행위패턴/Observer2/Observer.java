package 행위패턴.Observer2;

public interface Observer {
	public void update(String title, String news);
}
