하나의 객체의 상태가 변경되는 경우 다른 객체들에게
변경되었음을 알려주는 패턴

신문 정기 구독, 잡지 구독, 우유 배달, 공지사항 등 실제 생활에서도 
많은 케이스 확인 가능

======================================================================

Publisher 인터페이스 = Observer 들을 관리하는 Method 
Observer 인터페이스 = 정보를 업데이트 하는 Method

=======================================================================