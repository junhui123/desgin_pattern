package 행위패턴.Observer2;

//NewsMachine(Publisher) 클래스에서 notifyObserver() 호출 하면 Update를 호출
public class AnnualSubscriber implements Observer {

	private String newsString;
	private Publisher publisher;

	public AnnualSubscriber(Publisher publisher) {
		this.publisher = publisher;
		publisher.add(this);
	}

	@Override
	public void update(String title, String news) {
		this.newsString = title + "\n ----- \n" + news;
		display();
	}

	public void display() {
		System.out.println("AnnualSubscriber = " + newsString);
	}
}
