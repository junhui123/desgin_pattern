package 행위패턴.Observer2;

public interface Publisher {
	public void add(Observer observer);
	public void delete(Observer observer);
	public void notifyObserver();
}


