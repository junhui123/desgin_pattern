package 행위패턴.Observer2;

import java.util.ArrayList;

//정보를 제공하는 Publisher, 구독자 Observer 객체를 관리
public class NewsMachine implements Publisher {

	private ArrayList<Observer> observers;
	private String title;
	private String news;

	public NewsMachine() {
		observers = new ArrayList<Observer>();
	}

	@Override
	public void add(Observer observer) {
		observers.add(observer);
	}

	@Override
	public void delete(Observer observer) {
		observers.remove(observers.indexOf(observer));
	}

	@Override
	public void notifyObserver() {
		for (Observer observer : observers) {
			observer.update(title, news);
		}
	}

	public void setNewsInfo(String title, String news) {
		this.title = title;
		this.news = news;
		notifyObserver();
	}

	public String getTitle() {
		return title;
	}

	public String getNews() {
		return news;
	}

}
