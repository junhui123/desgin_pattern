package 행위패턴.Template_Method;

/**
 * Template Method는 사용 시 위험성 있음
 * 상위 abstract 클래스 변동 시 하위 클래스 안전성 보장 못함
 *
 */

public abstract class Worker {
    protected abstract void doit();
    //final 선언 이유? 상속 받은 클래스에서 변경 못하도록
    public final void work(){
        System.out.println("start");
        // abstract 메쏘드인 doit()을 호출
        // doit() Worker 상속받은 클래스에서 구현
        doit();
        System.out.println("end");
    }
}
