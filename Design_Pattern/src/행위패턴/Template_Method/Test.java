package 행위패턴.Template_Method;

public class Test {
	public static void main(String[] args) {
		Worker designer = new Designer();
		designer.work();
		Worker gamer = new Gamer();
		gamer.work();
	}
}