package 행위패턴.Template_Method;

public class Designer extends Worker {
    @Override
    protected void doit() {
        System.out.println("Design");
    }
}