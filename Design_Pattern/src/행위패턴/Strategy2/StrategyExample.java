package 행위패턴.Strategy2;


//동일한 명령에 다른 알고리즘을 적용 가능하게 하는 패턴이 전략패턴

/** 
 *  The classes that implement a concrete strategy should implement this. 
 *  The Context class uses this to call the concrete strategy. 
 */
interface Strategy {
	int execute(int a, int b);
};

/** Implements the algorithm using the strategy interface */ 
class Add implements Strategy {
	public int execute(int a, int b) {
		System.out.println("Called Add's execute()");
		return a + b; // Do an addition with a and b 
	}
}

class Subtract implements Strategy {
	public int execute(int a, int b) {
		System.out.println("Called Subtract's execute()");
		return a - b; // Do a subtraction with a and b
	}
};

class Multiply implements Strategy {
	public int execute(int a, int b) {
		System.out.println("Called Multiply's execute()");
		return a * b; // Do a multiplication with a and b
	}
};

// Configured with a ConcreteStrategy object and maintains
// a reference to a Strategy object
class Context {
	private Strategy strategy;

	public Context(Strategy strategy) {
		this.strategy = strategy;
	}

	public int executeStrategy(int a, int b) {
		return this.strategy.execute(a, b);
	}
};

/** Tests the pattern */
//동일한 명령에 다른 알고리즘을 적용 가능하게 하는 패턴이 전략패턴
//executeStrategy()라는 동일한 명령이지만 수행 결과는 다름 
//실질적 수행하는 알고리즘을 가진 메소드를 캡술화 하여 알고리즘의 변화에 유연하게 대처
//context = new Context(new Add()); new Add() 캡슐화
class StrategyExample {
	public static void main(String[] args) {
		Context context; // Three contexts following different strategies
		context = new Context(new Add());
		int resultA = context.executeStrategy(3, 4);
		
		context = new Context(new Subtract());
		int resultB = context.executeStrategy(3, 4);
		
		context = new Context(new Multiply());
		int resultC = context.executeStrategy(3, 4);
		
		System.out.println("Result A : " + resultA);
		System.out.println("Result B : " + resultB);
		System.out.println("Result C : " + resultC);
	}
};
