package 행위패턴.command;

/*the Command interface*/
public interface Command {
	void execute();
}