package 행위패턴.ChainofResponsibility3;

import java.util.List;
import java.util.Map;

public abstract class AbstractOrgDimensionValidator {

	/** 다음 실행할 검증기 */
	private AbstractOrgDimensionValidator next;

	/** 업로드 상태 */
	private List<String> uploadStatus;

	/**
	 * @return 다음 실행할 검증기 존재 여부
	 */
	private boolean hasNext() {
		return this.next != null;
	}

	/**
	 * @param next
	 */
	protected void setNext(AbstractOrgDimensionValidator next) {
		this.next = next;
	}

	/**
	 * @param uploadStatus
	 */
	protected void setUploadStatus(List<String> uploadStatus) {
		this.uploadStatus = uploadStatus;
	}

	/**
	 * @param orgRecord
	 */
	protected void executeNext(List<Map<String, String>> dataset) {
		if (hasNext()) {
			this.next.setUploadStatus(uploadStatus);
			this.next.validate(dataset);
		}
	}

	/**
	 * 각 검증기 내부에서 구현해야할 검증부
	 * 
	 * @param dataset
	 */
	abstract void validate(List<Map<String, String>> dataset);
}