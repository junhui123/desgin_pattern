package 행위패턴.ChainofResponsibility3;

import java.util.List;  
import java.util.Map;

public class OrgDimensionCodeValidator extends AbstractOrgDimensionValidator{

 @Override
 public void validate(List<Map<String, String>> dataset) {
        List<Map<String, String>> recordList = dataset;
        for (Map<String, String> orgRecord : recordList) {

            String data = orgRecord.get("Code");
            if (data != null) {
                System.out.println("코드 "+ data +"이(가) 검증되었습니다.");
            }
        }
        this.executeNext(dataset);
    }  

}

