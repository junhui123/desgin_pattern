Chain of Responsibility 패턴이란.

어떤 프로세스가 일어났을때 그 프로세스를 처리할 책임을 적당한 대상한테 넘겨주게 됩니다. Exception을 throw하는것도 비슷한 원리인데, Chain of responsibility pattern은 예외 만이 아니라 사건에 대한 처리역할을 적당한 대상에게 가도록 해주는 기법입니다. 이에 더하여 발생한 이벤트를 체인에 속한 여러 처리자에 걸쳐 처리할 수 있습니다.

Chain of Responsibility 패턴의 장점

요청을 보낸 쪽하고 받는쪽을 분리시킬 수 있습니다.

이 패턴은 하나의 클래스의 인스턴스들간의 체인이라기보다는 여러 클래스간에 걸쳐 이루어지는 일이기 때문에 구조가 다른 클래스에 대해서 낮은 결합도로 동일한 이벤트에 대한 핸들링을 가능하게 한다는 점에서 주목할 만 합니다.

사슬에 들어가는 객체를 바꾸거나 순서를 바꿈으로써 역할을 동적으로 추가/제거 할 수 있습니다. 상황에 따라 동적으로 핸들러를 추가하거나 제거할 수 있으며, 이러한 변화가 전체구조에 아무런 영향을 주지 않는다는 점에서 객체지향적인 목적을 달성한다고 볼 수 있습니다. 해당 패턴을 적용하게된 결정적인 이유가 바로 이때문 이었습니다.

보통은 일련의 프로세스에 대해 처리 클래스를 여러개 정의해두고 한데 묶어둔 후 사건이 일어나면 맨 앞의 클래스에게 '이거 처리해줘' 하고 넘겨주는식으로 체인의 스타트를 끊습니다. 그러면 묶어둔 순서대로 프로세스를 넘겨 받으면서 처리가 가능한 클래스를 만나면 해당 클래스가 처리를 하고 아니라면 다음 클래스에게 토스토스 하면서 프로세스를 처리하게 되는 것 입니다.

객체에서는 사슬의 구조를 몰라도 되고 그 사슬에 들어 있는 다른 객체에 대한 직접적인 레퍼런스를 가질 필요도 없기 때문에 객체를 단순하게 만들 수 있다는 장점이 있습니다.

Chain of Responsibility 패턴의 단점

요청이 반드시 수행된다는 보장이 없습니다.

만약 체인이 적절하게 구성되어 있지 않다면 프로세스가 사슬의 끝을 만날때 까지 처리 되지 않을 수도 있다는 것입니다. 반드시 프로세스가 처리 될 수 있도록 적절히 사슬의 순서를 구성해야 합니다.

요청을 처리하는데 걸리는 시간을 정확히 예측하기 함들다는 단점이 있습니다.

장점으로 언급되었던 구성의 유연함이 바로 단점이 되는 순간입니다. 실제로 요청을 처리해줄 객체가 체인의 어느 위치에 있을지는 동적으로 변동 될 수 있기 때문에 실시간 시스템과 같이 시간예측이 중요한 경우 이 패턴의 적용을 재검토 해야 할 것입니다.

실행시에 과정을 살펴보거나 디버깅하기가 힘들 수 있다는 단점