package 행위패턴.memento;

import java.util.ArrayList;
import java.util.List;

public class Test {
	public static void main(String[] args) {
		List<Memento> savedTimesList = new ArrayList<>();
		Life life = new Life();

		life.setTime("2000 B.C");
		savedTimesList.add(life.saveToMemento());
		life.setTime("2000 A.D");
		savedTimesList.add(life.saveToMemento());
		life.setTime("3000 A.D");
		savedTimesList.add(life.saveToMemento());

		System.out.println("===============================");
		for (Memento memento : savedTimesList) {
			life.restoreFromMemento(memento);
		}
	}
}
