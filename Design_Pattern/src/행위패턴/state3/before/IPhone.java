package 행위패턴.state3.before;

//http://blog.naver.com/2feelus/220667630164
/**
 * 아이폰에 전원버튼이 있는데 이걸 누를 때 상태의 변이가 일어나게 된다.
 * 
 * 1) 전원이 OFF일때 전원 버튼을 누르면 전원이 켜진다.(실제로는 길게 눌러야 하지만, 짧게 눌렀을떄 켜진다고 가정하자)
 * 
 * 2) 전원이 On, Display ON일때 전원버튼을 누르면 화면만 꺼진다.
 * 
 * 3) 전원이 On, Display OFF일때 전원버튼을 누르면 화면만 켜진다.
 * 
 * 상황이 많아지면 if문 증가 및 상태 변화 다양화 -> 개발 및 디버깅의 어려움!!
 */
public class IPhone {

	static enum State {
		POWER_OFF, POWER_ON_DISP_ON, POWER_ON_DISP_OFF
	};

	private State state = State.POWER_OFF;

	// 전원 ON,OFF에
	public void pressPowerButton() {

		if (state == State.POWER_OFF) {

			System.out.println("Power On");

			state = State.POWER_ON_DISP_ON;

		}

		else if (state == State.POWER_ON_DISP_ON) {

			System.out.println("Power On and Display OFF");

			state = State.POWER_ON_DISP_OFF;

		}

		else if (state == State.POWER_ON_DISP_OFF) {

			System.out.println("Power On and Display ON");

			state = State.POWER_ON_DISP_ON;

		}

		else {

			System.out.println("State Not Recognised");

		}

	}

}
