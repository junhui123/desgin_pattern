package 행위패턴.state3.after;

//세가지 상태에 대한 정의중 power on, display on 에 대한 정의

class PowerOnDispOnState implements State {
	
	private static State powerOnDispOnState = new PowerOnDispOnState();
	public IPhone iphone;

	public static State getInstance(IPhone iphone) {
		powerOnDispOnState.setIPhone(iphone);
		return powerOnDispOnState;

	}

	public void setIPhone(IPhone iphone) {
		this.iphone = iphone;
	}

	public void pressPowerButton() {
		System.out.println("IPhone PowerOnDispOnState");
		iphone.setState(PowerOnDispOffState.getInstance(iphone));
	}
}
