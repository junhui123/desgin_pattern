package 행위패턴.state3.after;

//세가지 상태에 대한 정의중 power on, display off 에 대한 정의

class PowerOnDispOffState implements State {

	private static State powerOnDispOffState = new PowerOnDispOnState();
	public IPhone iphone;

	public static State getInstance(IPhone iphone) {
		powerOnDispOffState.setIPhone(iphone);
		return powerOnDispOffState;
	}

	public void setIPhone(IPhone iphone) {
		this.iphone = iphone;
	}

	public void pressPowerButton() {
		System.out.println("IPhone PowerOnDispOffState");
		iphone.setState(PowerOnDispOnState.getInstance(iphone));
	}
}
