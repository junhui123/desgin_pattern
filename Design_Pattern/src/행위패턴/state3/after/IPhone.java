package 행위패턴.state3.after;

//행위 주체인 IPhone에 대한 상태 전이 함수인 pressPowerButton에 대한 정의
public class IPhone {
	private State state = null;

	public void pressPowerButton() {
		if (state == null) {
			state = PowerOffState.getInstance(this);
		}
		state.pressPowerButton();
	}

	public void setState(State state) {
		this.state = state;
	}
}
