package 행위패턴.state3.after;

public class PowerOffState implements State {
	
	private static State powerOffState = new PowerOffState();
	public IPhone iphone;
	
	public static State getInstance(IPhone iphone) {
		powerOffState.setIPhone(iphone);
		return powerOffState;
	}

	@Override
	public void setIPhone(IPhone iphone) {
		this.iphone = iphone;
	}

	@Override
	public void pressPowerButton() {
		System.out.println("IPhone PowerOffState");
		iphone.setState(PowerOnDispOnState.getInstance(iphone));
	}

}
