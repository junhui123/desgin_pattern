package 행위패턴.state3.after;

//행위주체 객체에 대한 설정메소드와 상태 변위 메소드에 대한 정의.

interface State {

	public void setIPhone(IPhone iphone);

	public void pressPowerButton();

}
