package 행위패턴.state3.after;

public class Test {
	public static void main(String[] args) {

		IPhone iphone = new IPhone();

		State PowerOff = PowerOffState.getInstance(iphone);
		State PowerOnDisOff = PowerOnDispOffState.getInstance(iphone);
		State PowerOnDispOn = PowerOnDispOnState.getInstance(iphone);

		iphone.setState(PowerOff);
		iphone.pressPowerButton();

		iphone.setState(PowerOnDisOff);
		iphone.pressPowerButton();

		iphone.setState(PowerOnDispOn);
		iphone.pressPowerButton();

	}
}
