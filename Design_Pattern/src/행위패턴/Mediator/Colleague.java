package 행위패턴.Mediator;

public abstract class Colleague {

	public IMediator mediator;

	public void setMediator(IMediator im) {
		this.mediator = im;
	}

	public void sendEvent(String name, String event) {
		mediator.sendEvent(name, event);
	}

	abstract public void fireEvent(String event);

	abstract public void receiveEvent(String name);

	abstract public String getName();
}
