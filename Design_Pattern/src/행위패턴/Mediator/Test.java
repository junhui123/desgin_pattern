package 행위패턴.Mediator;

/**
 * Mediator (중재자) 패턴. 클래스의 복잡도가 증가하면 중재자를 만들어 구현. 객체간의 M:N 관계를 중재자를 두어 1:1로
 * 만들어준다. 장점 : 객체간의 관계 코드가 중재자 클래스안에 집중됨으로 관리가 편해진다. ex)통보센터는 잘 만들어진 중재자 패턴의
 * 예이다.
 * 
 * 출처: http://metalbird.tistory.com/entry/MediatorPattern-중재자-패턴
 */
public class Test {
	public static void main(String[] args) {
		A a = new A();
		B b = new B();
		C c = new C();

		Mediator m = new Mediator();
		m.addColleague(a);
		m.addColleague(b);
		m.addColleague(c);

		m.sendEvent("B", "Hello");
		m.sendEvent("C", "Hello");
		a.fireEvent("Receive Mail");
		b.fireEvent("Receive Mail");
		c.fireEvent("Receive Mail");
	}
}
