package 행위패턴.Mediator;

public class C extends Colleague {

	String name = "C";

	@Override
	public void fireEvent(String event) {
		mediator.sendEvent(event, event);
	}

	@Override
	public void receiveEvent(String name) {
		System.out.println("Receive event from " + name);
	}

	@Override
	public String getName() {
		return name;
	}
}
