package 행위패턴.Mediator;

public class A extends Colleague {

  String name = "A";

  @Override
  public void fireEvent(String event) {
    mediator.sendEvent(event, event);
  }

  @Override
  public void receiveEvent(String name) {
    System.out.println("Receive event from " + name);
  }

  @Override
  public String getName() {
    return name;
  }
}
