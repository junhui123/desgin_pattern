package 행위패턴.Mediator2;

import java.util.Date;

//중재자
//User들을 자료구조를 통해서 관리 및 인터페이스 추가 기능 캡슐화 가능
public class ChatRoom {
	public static void showMessage(User user, String message) {
		System.out.println(new Date().toString() + " _ " + user.getName() + " : " + message);
	}
}
