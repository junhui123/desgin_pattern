package 행위패턴.Mediator2;

public class Test {
	public static void main(String[] args) {
		User robert = new User("Robert");
		User john = new User("John");
		
		robert.sendMessage("Hi!! John");
		john.sendMessage("Hi!! Robert");
	}
}
