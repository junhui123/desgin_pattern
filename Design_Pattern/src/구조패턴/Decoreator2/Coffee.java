package 구조패턴.Decoreator2;

//Component : 확장 가능성이 잇는 객체들의 인터페이스
//커피 전체를 의미 (추상적)
//The Coffee Interface defines the functionality of Coffee implemented by decorator
public interface Coffee {
	public double getCost(); // returns the cost of the coffee

	public String getIngredients(); // returns the ingredients of the coffee
}