package 구조패턴.Decoreator2;

//ConcreateDecorator
//실제 커피 첨가물
public class Milk extends CoffeeDecorator{

    public Milk(Coffee decoratedCoffee) {
        super(decoratedCoffee);
    }
	
	@Override
	public double getCost() {
		// TODO Auto-generated method stub
		return super.getCost() + 0.5;
	}

	@Override
	public String getIngredients() {
		// TODO Auto-generated method stub
		return super.getIngredients() + ingredientSeparator  + "Milk";
	}
}
