package 구조패턴.Decoreator2;

//Decorator
//커피 첨가물 전체를 의미 (추상적)
public abstract class CoffeeDecorator implements Coffee {

	protected final Coffee decoratedCoffee;
	protected String ingredientSeparator = ", ";

	public CoffeeDecorator(Coffee decoratedCoffee) {
		this.decoratedCoffee = decoratedCoffee;
	}

	@Override
	public double getCost() {
		// TODO Auto-generated method stub
		return decoratedCoffee.getCost();
	}

	@Override
	public String getIngredients() {
		// TODO Auto-generated method stub
		return decoratedCoffee.getIngredients();
	}

}