package 구조패턴.Decorator;

public class Test {
	public static void main(String[] args) {
		Decorator decorator = new Decorator();
		System.out.println(decorator.getMerong());

		Decorator child = new ChildDecorator(decorator);
		System.out.println(child.getMerong());

		Decorator child2 = new ChildDecorator(child);
		System.out.println(child2.getMerong());

		Decorator child3 = new ChildDecorator(child2);
		System.out.println(child3.getMerong());

		Decorator child4 = new ChildDecorator(new ChildDecorator(new ChildDecorator(new ChildDecorator(decorator))));

		System.out.println(child4.getMerong());
	}
}