package 구조패턴.Decorator;

//기존에 구현되어 있는 클래스에 기능을 추가하기 위한 패턴입니다.
//ConcreateCompoent : 실제 정의된 객체
public class Decorator {
	public String getMerong() {
		return "merong";
	}
}