package 구조패턴.Decorator;


/*
 * 하위 클래스는 상위클래스의 형식을 멤버변수로 가집니다.
 * ChildDecorator 는 Decorator를 멤버변수로 받습니다.
 * 일반적으로 생성자의 인자로 받아서 멤버변수로 쎄팅을 합니다.
 * 별도의 setter를 가지는 경우는 거의 없습니다. 
 * 
 * 하위 클래스는 상위클래스를 상속 받아 상위클래스의 메쏘드를 이용합니다.
 * 하위 클래스의 getMerong() 이라는 메쏘드는 상위 클래스의 getMerong()을 오버라이드하지만,
 * 내부적으로 상위클래스의 getMerong()을 사용하고 있습니다.
 */

/*
 * 일반 상속과 차이
 *  Decorator는 메쏘드의 확장 개념입니다.
 *  멤버 변수로 받은 객체의 메쏘드를 이용하여 그 메쏘드를 확장하는 것입니다.
 */

//ConcreateDecorator 
//Component에 추가할 서비스 실제 구현 및 기타 메소드 작성
public class ChildDecorator extends Decorator {
	private Decorator decorator;

	public ChildDecorator(Decorator decorator) {
		this.decorator = decorator;
	}

	@Override
	public String getMerong() {
		String sMerong = "@" + decorator.getMerong() + "@";
		System.out.println("str = " + sMerong);
		return sMerong;
	}
}