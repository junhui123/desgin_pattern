package 구조패턴.Adapter4;

//Adaptee : 인터페이스 개조가 필요한 기존의 인터페이스 정의
public interface Duck {
    public void quack();
    public void fly();
}


