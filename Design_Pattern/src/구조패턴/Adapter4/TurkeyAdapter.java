package 구조패턴.Adapter4;

//Adapter
public class TurkeyAdapter implements Duck {
	//위임 사용(클래스를 상속받지 않고 멤버 변수로 구현해 호출)
	Turkey turkey;

    /**
     * 오리의 행동으로 칠면조행동을 변환하기위한 칠면조 Reference를 받는다.
     *
     * @param turkey
     */
	public TurkeyAdapter(Turkey turkey) {
		this.turkey = turkey;
	}

	/**
	 * 오리의 행동을 칠면조에 위임
	 */
	@Override
	public void quack() {
		turkey.gobble();
	}

	/**
	 * 오리의 행동을 칠면조에 위임
	 */
	@Override
	public void fly() {
		turkey.fly();
	}
}
