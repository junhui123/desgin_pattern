package 구조패턴.Adapter4;

public interface Turkey {
	public void gobble();
	public void fly();
}
