package 구조패턴.Adapter4;

/**
 * Duck 객체가 모자라서 Turkey 객체를 대신 사용해양 하는 상황이라고 해보자.
 * 인터페이스가 다르기 때문에 Turkey객체를 바로 사용할 수는 없다.
 */
public class Test {
	public static void main(String[] args) {

		WildTurkey turkey = new WildTurkey();
		System.out.println("The turkey says...");
		turkey.gobble();
		turkey.fly();

		MallardDuck duck = new MallardDuck();
		System.out.println("The Duck says...");
		testDuck(duck);

		Duck turkeyAdapter = new TurkeyAdapter(turkey);
		System.out.println("The TurkeyAdapter says...");
		testDuck(turkeyAdapter);
	}

	public static void testDuck(Duck duck) {
		duck.quack();
		duck.fly();
	}
}
