package 구조패턴.Briedge2;

public class Test {
	public static void main(String[] args) {

		Display d1 = new Display(new StringDisplayImpl("Hello Korea."));
		Display d2 = new CountDisplay(new StringDisplayImpl("Hello World."));
		//CountDisplay는 Display를 상속받아 만들어진 클래스 이므로 하위는 상위를
		//알고 있으므로 사용 가능
		//CountDisplay d4 = (CountDisplay) new Display(new StringDisplayImpl("Test") );
		//이렇게 사용 되는 경우 Display는 상위 이므로 Display를 상속받은 하위 클래스가
		//어떤 것이 있는지 알지 못하므로 사용 불가
		CountDisplay d3 = new CountDisplay(new StringDisplayImpl("Hello. Universe"));
		
		d1.display();
		d2.display();
		d3.display();
		d3.multiDisplay(5);
	}
}
