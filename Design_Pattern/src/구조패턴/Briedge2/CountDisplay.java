package 구조패턴.Briedge2;

//기능의 클래스 계층 : 어떤 클래스를 확장하여 기능을 추가
//해당 예제에서는
//Display를 바탕으로 하여 다른 기능 추가 
public class CountDisplay extends Display {

	public CountDisplay(DisplayImpl impl) {
		super(impl);
	}

	public void multiDisplay(int times) {
		open();
		for (int i = 0; i < times; i++) {
			print();
		}
		close();
	}
}
