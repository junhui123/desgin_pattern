package 구조패턴.Briedge2;

//구현의 클래스 계층
//Display(브릿지)에서 사용하는 추상메소드
public abstract class DisplayImpl {
	public abstract void rawOpen();
	public abstract void rawClose();
	public abstract void rawPrint();
}
