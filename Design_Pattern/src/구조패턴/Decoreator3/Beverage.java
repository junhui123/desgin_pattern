package 구조패턴. Decoreator3;

//기본이 되는 Component 클래스
//음식 종류 마다 구현 클래스 생성
public abstract class Beverage {
	String description = "";

	public String getDescription() {
		return description;
	}

	// SubClass에서 구현해야 함.
	public abstract double cost();
}
