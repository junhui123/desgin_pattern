package 구조패턴. Decoreator3;

public class Whip extends CondimentDecorator {
	Beverage beverage;

	public String getName() {
		return "Whip Class";
	}
	
	public Whip(Beverage beverage) {
		this.beverage = beverage;
	}

	@Override
	public String getDescription() {
		return beverage.getDescription() + ", Whip";
	}

	@Override
	public double cost() {
		
		System.out.println(getName());
		
		double cost = .10 + beverage.cost(); 
		
		return cost;
	}

}