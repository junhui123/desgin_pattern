package 구조패턴. Decoreator3;

//커피의 첨가물(Condiment)을 나타내는 Abstract 클래스(Decorator 클래스)
//데코레이터 클래스의 형식은 그 클래스가 감싸고 있는 클래스의 형식을 반영한다.
//그러므로, Beverage 객체가 들어갈 자리에 들어갈 수 있어야 하므로 Beverage 클래스를 상속한다.
public abstract class CondimentDecorator extends Beverage {
	public abstract String getDescription();
}
