package 구조패턴. Decoreator3;

//음료를 구현하기 위해 Beverage 클래스를 상속한다.
public class DarkRoast extends Beverage {
	public DarkRoast() {
		description = "DarkRoast Coffee";
	}
	
	public String getName() {
		return "DarkRoast Class";
	}

	@Override
	public double cost() {
		// 음료 클래스에서는 첨가물 가격을 걱정할 필요가 없다.
		// 그냥 DarkRoast의 가격을 리턴하면 된다.
		
		System.out.println(getName());
		
		double cost = .99;
		return cost;
	}
}
