package 구조패턴. Decoreator3;

public class Test {

	public static void main(String[] args) {
		//Beverage beverage = new Espresso();
		//System.out.println(beverage.getDescription() + " $" + beverage.cost());

		// 아래와 같은 식으로 기본 음료에 첨가물들을 Wrapping해서 가격과 설명을 추가할 수 있다.
		Beverage beverage2 = new DarkRoast();
		//Mocha, Whip 데코레이터 객체
		beverage2 = new Mocha(beverage2);
		beverage2 = new Mocha(beverage2);
		beverage2 = new Whip(beverage2);
		
		//cost 호출
		//가장 바깥쪽의 Whip cost() 부터 호출 
		//가격 계산을 위임하고 계산이 끝나면 결가를 리턴
		// Whip Class -> Mocha Class -> Mocha Class
		System.out.println("$" + beverage2.cost());
		System.out.println(beverage2.getDescription());

//		Beverage beverage3 = new HouseBlend();
//		beverage3 = new Soy(beverage3);
//		beverage3 = new Mocha(beverage3);
//		beverage3 = new Whip(beverage3);
//		System.out.println(beverage3.getDescription() + " $" + beverage3.cost());
	}
}
