package 구조패턴. Decoreator3;

public class HouseBlend extends Beverage {
	public HouseBlend() {
		description = "HouseBlend Coffee";
	}

	@Override
	public double cost() {
		return .89;
	}
}
