package 구조패턴. Decoreator3;

public class SteamMilk extends CondimentDecorator {
	Beverage beverage;

	public SteamMilk(Beverage beverage) {
		this.beverage = beverage;
	}

	@Override
	public String getDescription() {
		return beverage.getDescription() + ", SteamMilk";
	}

	@Override
	public double cost() {
		return .10 + beverage.cost();
	}

}