package 구조패턴.Adapter;
import java.util.Enumeration;
import java.util.Iterator;


/**
 * * Adapter 패턴 : 한 인터페이스를 다른 인터페이스로 변환 (for 호환성) 인터페이스를 변경해서 클라이언트에서 필요로 하는
 * 인터페이스로 적응시키기 위한 용도.
 * 
 * Facade 패턴 : 인터페이스를 간단하게 바꿈 (for 간편함) 어떤 서브시스템에 대한 간단한 인터페이스를 제공하기 위한 용도.
 * 
 * Decorator 패턴 : 인터페이스를 바꾸지 않고 책임(기능)만 추가함 (for 기능 추가)
 * 
 * 객체를 감싸서 새로운 행동을 추가하기 위한 용도.
 *
 */
public class IteratorToEnumeration implements Enumeration<String>{
    private Iterator<String> iter;
    public IteratorToEnumeration(Iterator<String> iter) {
        this.iter = iter;
    }
    public boolean hasMoreElements() {
        return iter.hasNext();
    }
    public String nextElement() {
        return iter.next();
    }
}