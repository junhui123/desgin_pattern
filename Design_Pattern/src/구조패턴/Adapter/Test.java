package 구조패턴.Adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

public class Test {
    public static void goodMethod(Enumeration<String> enu){
        while (enu.hasMoreElements()) {
            System.out.println(enu.nextElement());
        }
    }
 
    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        list.add("이은결");
        list.add("Kevin parker");
        list.add("David Blaine");
        
        Enumeration aa = Collections.enumeration(list);
//        Enumeration<String> ite = new IteratorToEnumeration(list.iterator());
        Test.goodMethod(aa);
   }
}