package 구조패턴.Facade;


/*
 * 여러가지를 하나로 간주해서 다룸
 * 자동차 = 바퀴+엔진+오디어+시트+ .. => 통틀어 자동차로 지칭하는것이 편함
 * 따로 차량에 내장된 오디오라고 명확히 하는것은 귀찮음 및 불필요
 * 
 * TV가 Home에 종속되지 않을 수 있음. TV를 보기위해서 폰이나 컴퓨터를 사용 가능
 * TV시청 기능을 분리 시키지 않는 다면 폰 클래스 설계 시 TV 기능 다시 구현 해야 하는 문제 발생 
 */

public class Home {
	private Audio audio;
	private Light light;
	private TV tv;

	public Home(Audio audio, Light light, TV tv) {
		this.audio = audio;
		this.light = light;
		this.tv = tv;
	}

	public void enjoyTv() {
		System.out.println("==불을 밝게하고 TV보기.");
		light.setLightness(2);
		tv.turnOn();
	}

	public void enjoyMusic() {
		System.out.println("==불을 약간 어둡게하고 음악듣기.");
		light.setLightness(1);
		audio.play();
	}

	public void goOut() {
		System.out.println("==TV끄고, 음악도 끄고, 불도 끄고 외출하기.");
		if (tv.isTurnedOn()) {
			tv.turnOff();
		}
		if (audio.isPlaying()) {
			audio.stop();
		}
		light.setLightness(0);
	}
}
