package 구조패턴.Facade;

public class Test {
	public static void main(String[] args) {
		TV tv = new TV();
		Audio audio = new Audio();
		Light light = new Light();
		
		//tv, audio, light 인스턴스는 Home을 생성하는 용도 이외에는 사용하지 않음
		//저차원 클래스 조합
		Home home = new Home(audio, light, tv);

		home.enjoyTv();
		home.enjoyMusic();
		home.goOut();
	}
}
