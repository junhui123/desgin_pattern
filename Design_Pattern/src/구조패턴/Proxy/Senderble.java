package 구조패턴.Proxy;


//Subject : RealSubJect, Proxy 공통의 인터페이스. RealSubJect 요청하는곳에 Proxy 사용 가능
public interface Senderble {
	void setFromName(String name);
	void setToName(String name);
	String getFromName();
	String getToName();
	void send() throws InterruptedException;
}
