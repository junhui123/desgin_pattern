package 구조패턴.Proxy;


//RealSubJect : 프록시가 대표하는 실제 객체
public class Mail implements Senderble {

	private String toName;
	private String fromName; 
	
	public Mail() throws InterruptedException {
		System.out.println("Mail 객체 생성 시작...");
		Thread.sleep(1000);
		System.out.println("Mail 객체 생성 끝...");
	}

	@Override
	public void setFromName(String name) {
		this.fromName = name;
	}

	@Override
	public void setToName(String name) {
		this.toName = name;
	}

	@Override
	public String getFromName() {
		return this.fromName;
	}

	@Override
	public String getToName() {
		return this.toName;
	}

	@Override
	public void send() throws InterruptedException {
		System.out.println("Mail Send");
	}

}
