package 구조패턴.Proxy;

public class Test {
	//MailProxy는 보내는 사람(formName)과 받는 사람(toName)을 설정하고
	//실제 메일을 보낼때 Mail 객체를 생성하여 위임 시키게 됩니다.
	public static void main(String[] args) throws InterruptedException {
		MailProxy mailProxy = new MailProxy();
		mailProxy.setFromName("피터팬");
		mailProxy.setToName("팅커벨");
		mailProxy.send();
	}

}
