package 구조패턴.Proxy;

//Proxy : 실제를 참조할 대리자 
public class MailProxy implements Senderble {

	private Mail mail;
	private String toName;
	private String fromName;
	
	@Override
	public void setFromName(String name) {
		if ( mail != null) {
			mail.setFromName(name);
		}
		this.fromName = name;
	}

	@Override
	public void setToName(String name) {
		if(mail != null) {
			mail.setToName(name);
		}
		this.toName = name;
	}

	//실제 Mail 객체를 거치지 않고 대리(MailProxy)만으로 사용 가능
	@Override
	public String getFromName() {
		return fromName;
	}

	//실제 Mail 객체를 거치지 않고 대리(MailProxy)만으로 사용 가능
	@Override
	public String getToName() {
		return toName;
	}

	
	@Override
	//여러 사용자가 사용하기 때문에 객체가 여러번 생성 가능 하므로 동기화 필요
	public synchronized void send() throws InterruptedException {
		if(mail == null ) {
			mail = new Mail();
		}
		mail.send();
	}
}
