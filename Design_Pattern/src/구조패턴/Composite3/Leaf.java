package 구조패턴.Composite3;

//종단 클래스, 상속받은 인터페이스의 함수들을 구현한 구현체
public class Leaf implements Component{
	private String name;

	public Leaf(String name) {
		this.name = name;
	}

	@Override
	public void show() {
		System.out.println(name);
	}
}
