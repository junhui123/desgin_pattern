package 구조패턴.Composite3;

import java.util.ArrayList;
import java.util.List;


//오브젝트의 집합, 같은 인터페이스를 상속받은 다른 오브젝트를 자식으로 갖습니다.
//상속받은 메소드를 구현하나, 일반적으로 포함하고 있는 자식들에게 '행위(처리)'를 위임하는 형태입니다.
public class Composite implements Component {

	private List<Component> childComponents = new ArrayList<Component>();

	public void add(Component c) {
		childComponents.add(c);
	}

	public void remove(Component component) {
		childComponents.remove(component);
	}

	@Override
	public void show() {
		for (Component component : childComponents) {
			component.show();
		}
	}
}
