package 구조패턴.Composite3;

//모든 구성 컴포넌트의 추상체이며, recursive(재귀적)한 구조를 표현하기위한 부모 컴포넌트(interface)
public interface Component {
	public void show();
}
