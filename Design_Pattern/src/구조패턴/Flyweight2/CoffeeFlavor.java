package 구조패턴.Flyweight2;

//ConcreateFlyweight를 생성하는 ConcreateFlyweight 객체
public class CoffeeFlavor implements CoffeeOrder {
	private final String flavor;
	
	public CoffeeFlavor(String flavor) {
		super();
		this.flavor = flavor;
	}
	
	public String getFlavor(){
		return this.flavor;
	}

	@Override
	public void serveCoffee(CoffeeOrderContext context) {
		System.out.println("Serving Coffee flavor " + flavor +" to table number " + context.getTable());
	}

}
