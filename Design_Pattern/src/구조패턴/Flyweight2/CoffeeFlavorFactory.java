package 구조패턴.Flyweight2;

import java.util.HashMap;
import java.util.Map;

//FlyweightFactory 객체
//Factory를 통해 생성한 객체 관리
public class CoffeeFlavorFactory {

	private Map flavors = new HashMap();

	public CoffeeFlavor getCoffeeFlavor(String flavorName) {
		CoffeeFlavor flavor = (CoffeeFlavor) flavors.get(flavorName);
		if (flavor == null) {
			flavor = new CoffeeFlavor(flavorName);
			flavors.put(flavorName, flavor);
		}
		return flavor;
	}

	public int getTotalCoffeeFlavorsMade() {
		return flavors.size();
	}
}
