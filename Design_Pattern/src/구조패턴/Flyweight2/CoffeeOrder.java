package 구조패턴.Flyweight2;

//Flyweight 객체 인터페이스
public interface CoffeeOrder {
	public void serveCoffee(CoffeeOrderContext context);
}
