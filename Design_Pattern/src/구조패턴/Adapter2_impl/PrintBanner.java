package 구조패턴.Adapter2_impl;

//PrintBanner 클래스가 어댑터의 역할
public class PrintBanner extends Banner implements Print {

	public PrintBanner(String string) {

		super(string);

	}

	public void printWeak() {

		showWithParen();

	}

	@Override
	public void printStong() {
		showWithAster();
	}

}