package 구조패턴.Adapter2_impl;

//타겟
//Adaptee : 개조가 필요한 인터페이스 정의
public interface Print {

	public abstract void printWeak();

	public abstract void printStong();

}