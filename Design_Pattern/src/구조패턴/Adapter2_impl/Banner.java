package 구조패턴.Adapter2_impl;

//이미 제공되는 클래스
public class Banner {
	private String string;

	public Banner(String string) {

		this.string = string;

	}

	public void showWithParen() {

		System.out.println("(" + string + ")");

	}

	public void showWithAster() {

		System.out.println("*" + string + "*");
	}

}
