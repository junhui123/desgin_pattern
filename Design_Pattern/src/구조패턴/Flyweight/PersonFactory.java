package 구조패턴.Flyweight;

import java.util.HashMap;
import java.util.Map;

public class PersonFactory {
	private static Map<String, Person> map = new HashMap<String, Person>();

	//이미 있는 놈인지 체크를 하고(1단계) 없으면 새로 만듭니다.(2단계) 1,2 단계가 합쳐져서 하나의 synchronized
	public synchronized static Person getPerson(String name) {
		if (!map.containsKey(name)) {
			//Person 생성자 == private, Person 클래스가 PersonFactory 안에 위치하므로
			//new 연산자로 생성 가능
			//외부에서 호출 시 생성자가 private 이므로 불가능
			Person tmp = new Person(name);
			map.put(name, tmp);
		}
		return map.get(name);
	}

	public static class Person {
		private final String name;

		// 생성자 Private
		private Person(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}
}

/**
 * 생성자 Private (http://aroundck.tistory.com/154) static method 와 static field 만을
 * 모아 놓은 클래스를 만들 필요가 종종 있습니다. 예를 들자면, Utility class 들이 있죠, ( 대표적 ex) Math ) 이런
 * 친구들은 인스턴스를 생성하지 못하게 설계되어야 합니다. 인스턴스 생성이 무의미하기 때문이죠.
 * 
 * 출처: http://stevenjsmin.tistory.com/107 [Steven J.S Min's Blog] private 으로
 * 생성자를 선언하게 되면 인스턴스 생성이 불가능하게 된다. 뿐만 아니라 생성자를 호출할 수 없으므로 서브클래스를 만들 수 없게 된다.
 * 
 **/
