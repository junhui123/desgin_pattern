package 구조패턴.Facade2;

class Computer {
    private CPU cpu;
    private Memory memory;
    private HardDrive hardDrive;
 
    public Computer() {
        this.cpu = new CPU();
        this.memory = new Memory();
        this.hardDrive = new HardDrive();
    }
 
    public void run() {
    	//내부적으로 처리가 필요한 작업은 클라이언트가 알아야할 필요가 없음
        cpu.processData();
        memory.load();
        hardDrive.readdata();
    }
}


