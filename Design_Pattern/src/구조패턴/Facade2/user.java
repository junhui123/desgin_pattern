package 구조패턴.Facade2;

public class user {
	public static void main(String[] args) {
		Computer computer = new Computer();
		//외부에서 보이는 작업
		//처리하고 결과만 보여줌
		computer.run();
	}
}
