package 구조패턴.Facade3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class PageMaker {
	private PageMaker() {// 인스턴스를 안만듬 그래서 private

	}

	public static void makeWelcomePage(String mailaddr, String filename) {

		try {
			// -------------------------Database
			// 클래스부분-------------------------------
			// Database.getProperties는 static 메소드라서 이렇게 인스턴스 없이 call할 수 있음.
			Properties mailprop = Database.getProperties("maildata");
			// 이줄까지 하면 file로부터 값을 읽어서 해쉬테이블에 넣었음.
			// -----------------------------------------------------------------------------

			// -------------------------HtmlWriter
			// 클래스부분--------------------------------
			String username = mailprop.getProperty(mailaddr);
			// 이줄가지 하면 mailaddr를 키값으로 해쉬테이블에서 데이터를 찾음.

			HtmlWriter writer = new HtmlWriter(new FileWriter("C:\\eclipse_workspace\\Design_Pattern\\src\\Facade3\\"+filename));
			// new FileWriter("welcome.html")과 같음
			// 생성하면 바로 파일이 만들어짐

			// ----------파일에 내용을 쓴느 부분.-----------
			writer.title("Welcome to " + username + " 's page!");
			writer.paragraph("메일을 기다리고 있습니다.");
			writer.mailto(mailaddr, username);

			// ----------파일을 닫음.-----------
			writer.close();
			// -------------------------------------------------------------------------------

			System.out.println(filename + " is created for " + mailaddr + " (" + username + ")");

		} catch (IOException e) {

			e.printStackTrace();

		}
	}

}
