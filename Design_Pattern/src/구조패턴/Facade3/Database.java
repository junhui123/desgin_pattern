package 구조패턴.Facade3;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Database {

	private Database() { 
		// new 사용 인스턴스 생성 못하도록 private 선언
		// 인스턴스 생성이 무의미 한 경우에 private 생성자 사용
		// private이므로 인스턴스 없이 사용하기 위해 static 클래스, 멤버를 선언해서 사용
		
		/*
		 * static 메소드와 static 필드만을 모아 놓은 클래스를 만들 필요가 종종 있을 것이다. 그런 유틸리티(Utility)
		 * 클래스들은 인스턴스를 생성하지 못하게 설계되었다. 인스턴스 생성이 무의미 하기 때문이다.(Math, Arrays 등) 그러나
		 * 그런 클래스일지라도 명시적으로 지정한 생성자가 없을 때는 컴파일러가 디폴트(default) 생성자를 만들어 준다. 클래스
		 * 사용자 입장에서는 이 생성자가 다른 것과 차이가 없으며, 인스턴스 생성이 가능한 클래스로 오인될 수 있다.
		 * 
		 * 우리가 private 생성자를 정의하면 인스턴스 생성이 불가능한 클래스를 만들 수 있다.
		 * 
		 * // 인스턴스 생성이 불가능한 유틸리티 클래스 public class UtilityClass { // 디폴트 생성자가
		 * 자동으로 생기는 것을 방지한다. private UtilityClass() { throw new
		 * AssertionError(); } ... // 이하 생략 } 명시적으로 정의한 생성자가 private 이므로 이 클래스
		 * 외부에서는 생성자 호출이 불가능 하다.
		 */
	}

	public static Properties getProperties(String dbname) {

		// 데이터 베이스 이름에서 Properties를 얻는다.
		String filename = "C:\\eclipse_workspace\\Design_Pattern\\src\\Facade3\\"+dbname + ".txt";
		Properties prop = new Properties();// 해쉬테이블을 만듬.

		try {

			prop.load(new FileInputStream(filename));// 파일로부터 key와 data값을 해쉬테이블에 넣음

		} catch (IOException e) {

			System.out.println("Warning : " + filename + " is not found.");

		}

		return prop;
	}

}
