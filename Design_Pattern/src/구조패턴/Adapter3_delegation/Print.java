package 구조패턴.Adapter3_delegation;

//타겟
//위임을 사용 시 추상 클래스로 정의
public abstract class Print {

	public abstract void printWeak();

	public abstract void printStrong();
}
