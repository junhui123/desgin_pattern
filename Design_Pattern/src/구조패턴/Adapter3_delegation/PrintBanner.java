package 구조패턴.Adapter3_delegation;

//PrintBanner 클래스가 어댑터의 역할
public class PrintBanner extends Print {

	private Banner banner;

	public PrintBanner(String string) {

		this.banner = new Banner(string);

	}

	public void printWeak() {

		banner.showWithParen();

	}

	public void printStrong() {

		banner.showWithAster();

	}

}