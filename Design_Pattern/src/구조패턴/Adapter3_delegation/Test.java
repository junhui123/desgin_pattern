package 구조패턴.Adapter3_delegation;

public class Test {
	public static void main(String[] args) {
		/**
		 * PrintBanner P = new PrintBanner("Hello")
		 * 
		 * PrintBanner를 이용하지 않고 Print 인터페이스의 메소드를
		 * 이용하고 있음을 표시 및 프로그램 의도 분명해짐
		 */
		Print p = new PrintBanner("hello");
		p.printWeak();
		p.printStrong();
	}
}
