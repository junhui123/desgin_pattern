package 구조패턴.Flyweight3;

import java.util.Hashtable;

//Flyweight Factory
public class PersonFactory {
	
	//Hashtable은 내부적으로 synchronized 
	private Hashtable<Integer, Person> workers = new Hashtable<Integer, Person>();
	
	public Person get(int pid) {
		//아래의 작업은 일반적으로 synchronized 되어 작동해야함
		//객체가 없음을 확인 후 객체 생성 하는 작업이 여러 스레드에서 수행하는 경우 
		//동기화 오류가 발생가능
		//Hashtable 사용 시 객체 확인, 객체 생성 각각의 단계에 따라 Hashtable 메소드 
		//내부적으로 syncrhonized 처리가 됨
		Person person = workers.get(pid);
		if (person == null) {
			person = new Worker(pid);
			workers.put(pid, person);
		}
		return person;
	}

	// 총 일꾼수.
	public int size() {
		return workers.size();
	}

}
