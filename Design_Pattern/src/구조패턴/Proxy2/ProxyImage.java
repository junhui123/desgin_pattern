package 구조패턴.Proxy2;

//Proxy Class - RealSubject Class(RealImage)를 참조
//Client에서 메소드 호출에 대해서 처리, 객체 생성, 보안, 리모트 콜 등 작업 추가
//on System B
public class ProxyImage implements Image {
	private String fileName;
	private Image image;
	
	public ProxyImage(String fileName) {
		this.fileName = fileName;
	}
	
	@Override
	public void displayImage() {
		if(image == null ) {
			image = new RealImage(fileName);
		}
		image.displayImage();
	}
}
