package 구조패턴.Proxy2;

//Client Class
public class Test {
	public static void main(String[] args) {
		Image image1 = new ProxyImage("Photo1");
		Image image2 = new ProxyImage("Photo2");

		image1.displayImage();
		image2.displayImage();
	}
}
