package 구조패턴.Proxy2;

//RealSubject Class
//on System A
public class RealImage implements Image {
	private String fileName;

	public RealImage(String fileName) {
		this.fileName = fileName;
		loadImageFromDisk();
	}

	@Override
	public void displayImage() {
		System.out.println("Display = " + fileName);
	}

	private void loadImageFromDisk() {
		System.out.println("Loading  = " + fileName);
	}
}
