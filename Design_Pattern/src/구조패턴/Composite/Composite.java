package 구조패턴.Composite;

public class Composite extends Component {
	public Composite(String componentName) {
		super(componentName);
	}

	@Override
	public void add(Component c) {
		children.add(c);
	}
}
