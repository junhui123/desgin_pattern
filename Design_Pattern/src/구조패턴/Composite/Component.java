package 구조패턴.Composite;

import java.util.ArrayList;
import java.util.List;


/*
 * 파일 데이터와 같은 일반적인 트리 구조의 데이터 타입을 만드는 것이 Composite 패턴
 * 클라이언트는 Component 인터페이스/추상클래스를 이용해서 개별/복합객체를 사용할 수 있다.
   객체(Leaf)와 복합객체(Composite)는 Component를 상속받으며 해당 메소드 중 연관된 메소드만 재정의한다.
   (같은 인터페이스를 구현하는 두 종류(Composite 와 Leaf)의 클래스 객체로 구성)
    
    구현방법
    1) 객체의 구성 및 상하위 체계를 파악한다.
	2) 파악된 객체들을 트리 구조로 설계한다.
	3) 객체와 복합객체는 공통으로 사용할 수 있는 메소드가 정의된 인터페이스/추상클래스를 구현/상속한다.
	
	http://jusungpark.tistory.com/26
	
	주요 사용 분야 (!!! 계층구조)
	대부분의 OS에서 파일구조가 계층적이고, UI의 컴포넌트들이 이루는 구조또한 계층적이다
	네트워크의 형태또한 계층적일 수 있으며 여러 형태의 문서들또한 계층을 이루어 관리한다. (XML, HTML, etc...)
	GUI는 프레임이나 패널과 같은 최상위 구성요소가 있고, 그안에 메뉴나 텍스트틀, 스크롤, 버튼이 있지만 렌더링하는 입장에서는 이런것들을 부분으로 나누어 생각하기 보다는 전체를 하나로 묶어서 생각하는것이 효율적이다. 즉 최상위 구성요소가 렌더링을 시작되면 나머지 부분은 구성요소에서 알아서 처리하록 할 수 있다.
 *
 */

public abstract class Component {
	private String componentName;
	protected List<Component> children = new ArrayList<Component>();

	public Component(String componentName) {
		this.componentName = componentName;
	}

	public String getComponentName() {
		return componentName;
	}

	public abstract void add(Component c);

	public List<Component> getChildren() {
		return children;
	}

	public String getString() {
		return getString(0);
	}

	private String getString(int depth) {
		StringBuffer sb = new StringBuffer();
		if (this instanceof Composite) {
			for (int i = 0; i < depth; i++) {
				sb.append("  ");
			}
			sb.append("+" + getComponentName() + "\n");
			for (Component comp : children) {
				sb.append(comp.getString(depth + 1));
			}
		} else {
			for (int i = 0; i < depth; i++) {
				sb.append("  ");
			}
			sb.append("-" + getComponentName() + "\n");
		}
		return sb.toString();
	}
}
