package 구조패턴.Composite2;

/**
 * File 클래스는 Node 인터페이스를 구현하면 끝입니다. 자신은 자식 요소를 가질 필요가 없기 때문이죠.
 * 
 * Leaf 요소는 Component로 지정된 인터페이스를 구현한 겁니다. 위의 예제 코드에서 File 클래스가 이 역할을 담당하고 있었습니다.
 * 
 */
class File implements Node {
	private String name;

	@Override
	public String getName() {
		return name;
	}
}
