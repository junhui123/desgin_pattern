package 구조패턴.Composite2;

/**
Node 클래스는 기본적인 파일 및 디렉토리의 근간이라고 가정합니다.
모든 파일과 디렉토리는 이름을 가지고 있을테니 이름을 반환할 getName() 메소드를 가집니다.
*/

/**
 * 모든 표현할 요소들의 추상적인 인터페이스를 말합니다. 위의 예제 코드에서 Node 인터페이스가 이 역할을 담당하고 있었습니다.
 */
public interface Node {
	public String getName();
}
