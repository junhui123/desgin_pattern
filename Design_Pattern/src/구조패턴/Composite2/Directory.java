package 구조패턴.Composite2;

import java.util.ArrayList;
import java.util.List;

/**
 * Directory 클래스는 Node 인터페이스를 구현하는 것 외에도 자식 요소를 담아둘 List가 필요합니다.
 * 
 * Composite 요소는 Component 요소를 자식으로 가집니다.
 *  따라서 Component 요소를 관리하기 위한 메소드들을 추가적으로 구현해줘야겠죠. 
 *  위의 예제 코드에서는 Directory 클래스가 이 역할을 담당하고 있었습니다.
 * 
 */
class Directory implements Node {
	private String name;
	private List<Node> children = new ArrayList<>();

	// ...
	@Override
	public String getName() {
		return name;
	}

	public void add(Node node) {
		children.add(node);
	}
	
	public void printList() {
		System.out.println(children);
	}
}
