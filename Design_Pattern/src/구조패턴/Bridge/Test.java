package 구조패턴.Bridge;

//Client
public class Test {
	   public static void main(String[] args) {
		   
		   //Aggregation(포함) 관계 이므로 
		   //Abstraction은 implementor를 여러개 가질 수 있음.
	       Shape[] shapes = new Shape[] {
	           new CircleShape(1, 2, 3, new DrawingAPI1()),
	           new CircleShape(5, 7, 11, new DrawingAPI2()),
	       };
	  
	       for (Shape shape : shapes) {
	           shape.resizeByPercentage(2.5);
	           shape.draw();
	       }
	   }
}
