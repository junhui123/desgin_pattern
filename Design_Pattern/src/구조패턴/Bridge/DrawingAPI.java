package 구조패턴.Bridge;

/** "Implementor" */
interface DrawingAPI {
	public void drawCircle(double x, double y, double radius);
}
