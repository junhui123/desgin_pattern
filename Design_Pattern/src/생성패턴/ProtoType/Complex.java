package 생성패턴.ProtoType;

import java.util.Date;

public class Complex implements Cloneable {

	private String complex;
	private Date date;

	public Complex(String complex) {
		this.complex = complex;
	}

	public String getComplex() {
		return complex;
	}

	public void setComplex(String complex) {
		this.complex = complex;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = new Date(date.getTime());
	}

	//복체 오퍼레이션
	@Override
	protected Object clone() throws CloneNotSupportedException {
		Complex tmp = (Complex) super.clone();
		return tmp;
	}
}
