package 생성패턴.ProtoType;

import java.util.Date;

//클래스로부터 인스턴스를 만드는 것이 아니고, 인스턴스에서 새로운 인스턴스를 만든다.(복사해서 사용)
//http://outliers.tistory.com/entry/%EB%94%94%EC%9E%90%EC%9D%B8-%ED%8C%A8%ED%84%B4Design-Pattern-Prototype-Pattern
public class Test {
	public static void main(String[] args) {
		Complex com = new Complex("매우 복잡한 정보");
		try {
			Complex cloned1 = (Complex) com.clone();
			cloned1.setDate(new Date(2008, 0, 1));

			Complex cloned2 = (Complex) com.clone();
			cloned2.setDate(new Date(2008, 2, 1));

			System.out.println(cloned1.getDate());
			System.out.println(cloned2.getDate());
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	}
}
