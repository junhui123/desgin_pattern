package 생성패턴.A_Factory_Functional2;

public class Circle implements Shape {
  @Override
  public void draw() {
     System.out.println("Inside Circle::draw() method.");
  }
}