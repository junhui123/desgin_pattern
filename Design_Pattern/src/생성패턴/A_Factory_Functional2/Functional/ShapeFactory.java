package 생성패턴.A_Factory_Functional2.Functional;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import 생성패턴.A_Factory_Functional2.Circle;
import 생성패턴.A_Factory_Functional2.Rectangle;
import 생성패턴.A_Factory_Functional2.Shape;

public class ShapeFactory {

  final static Map<String, Supplier<Shape>> map = new HashMap<>();
  static {
    map.put("CIRCLE", Circle::new);
    map.put("RECTANGLE", Rectangle::new);
  }

  public Shape getShape(String shapeType) {
    Supplier<Shape> shape = map.get(shapeType.toUpperCase());
    if (shape != null) {
      return shape.get();
    }
    throw new IllegalArgumentException("No such shape " + shapeType.toUpperCase());
  }
}
