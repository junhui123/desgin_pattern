package 생성패턴.A_Factory_Functional2.Functional;

import java.util.function.Supplier;

public class Test {
  public static void main(String[] args) {
    Supplier<ShapeFactory> shapeFactory =  ShapeFactory::new;
    shapeFactory.get().getShape("circle").draw();
    shapeFactory.get().getShape("rectangle").draw();
  }
}
