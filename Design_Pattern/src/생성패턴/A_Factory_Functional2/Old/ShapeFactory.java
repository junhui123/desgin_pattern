package 생성패턴.A_Factory_Functional2.Old;

import 생성패턴.A_Factory_Functional2.Circle;
import 생성패턴.A_Factory_Functional2.Rectangle;
import 생성패턴.A_Factory_Functional2.Shape;

public class ShapeFactory {
  
  public Shape getShape(String shapeType) {
    if(shapeType == null){
      return null;
   }
   if(shapeType.equalsIgnoreCase("CIRCLE")){
      return new Circle();
   } else if(shapeType.equalsIgnoreCase("RECTANGLE")){
      return new Rectangle();         
   }       
   return null;
  }
  
}
