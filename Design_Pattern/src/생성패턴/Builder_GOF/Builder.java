package 생성패턴.Builder_GOF;

interface Builder {
	void makeArm();
	void makeLeg();
	Hero getResult();
}
