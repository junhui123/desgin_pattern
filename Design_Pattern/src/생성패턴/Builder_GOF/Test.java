package 생성패턴.Builder_GOF;

//Director를 이용해 Hero를 찍어내는 Test클래스
public class Test {
	public static void main(String[] args) {
		
		//Product 구현을 위한 요소 구현, 빌더 합성 
		Builder builder = new BatmanBuilder();
		
		//Director 빌더 관리
		Director director = new Director(builder);
		director.build();
		
		//Product
		Hero hero = director.getHero();
		hero.showResult();
	}
}
