package 생성패턴.Builder_GOF;

//Product : 생성할 객체 표현
public class Hero {
	private String armSource;
	private String legSource;
	private String name;

	public Hero(String name) {
		super();
		this.name = name;
	}

	public void setArmSource(String armSource) {
		this.armSource = armSource;
	}

	public void setLegArmSource(String legSource) {
		this.legSource = legSource;
	}

	public void showResult() {
		System.out.println(armSource + " and " + legSource + ", name = " + name);
	}
}
