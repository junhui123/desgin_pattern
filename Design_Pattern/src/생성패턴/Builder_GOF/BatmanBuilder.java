package 생성패턴.Builder_GOF;

//concreate Builder : 
//Builder는 비교적 세부적인 사항들을 다룹니다. 이에 비해 Director는 좀 더 포괄적인 과정은 다룹니다.
public class BatmanBuilder implements Builder {
	private Hero batman;
	BatmanBuilder() {
		batman = new Hero("BatMan");
	}

	@Override
	public void makeArm() {
		batman.setArmSource("batman Arm");
	}

	@Override
	public void makeLeg() {
		batman.setLegArmSource("batman Leg");
	}

	@Override
	public Hero getResult() {
		return batman;
	}

}
