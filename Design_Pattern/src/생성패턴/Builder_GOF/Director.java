package 생성패턴.Builder_GOF;

//Builder 관리 
//Director는 좀 더 포괄적인 과정
//Director는 다른 Hero를 만드는데도 활용할 수 있지만, Builder는 각각의 Hero에 국한됩니다.
//전체적인 로직(포괄적인 과정인 build() 메쏘드)
public class Director {
	private Builder builder;

	public Director(Builder builder) {
		this.builder = builder;
	}

	public void build() {
		builder.makeArm();
		builder.makeLeg();
	}

	public Hero getHero() {
		return builder.getResult();
	}
}
