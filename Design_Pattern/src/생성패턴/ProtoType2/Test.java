package 생성패턴.ProtoType2;

//Client
public class Test {
	public static void main(String[] args) {
		Manager manager = new Manager();
		MessageBox mbox = new MessageBox('*');
		MessageBox sbox = new MessageBox('/');
		manager.register("strong message", mbox);
		manager.register("slash box", sbox);

		Product p1 = manager.create("strong message");
		p1.use("Hello world");
		Product p2 = manager.create("slash box");
		p2.use("Hello world");
	}
}
