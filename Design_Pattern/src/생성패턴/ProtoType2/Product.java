package 생성패턴.ProtoType2;

//ProtoType = 자신의 복체에 필요한 메소드 정의
public interface Product extends Cloneable{
	public abstract void use(String s);
	public abstract Product createClone();
}
