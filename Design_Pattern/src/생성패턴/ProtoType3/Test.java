package 생성패턴.ProtoType3;

//Client
public class Test {
	public static void main(String args[]) throws CloneNotSupportedException {
		Prototype prototype = new ProtoTypeImpl(1000);

		for (int i = 1; i < 10; i++) {
			Prototype tempotype = (Prototype) prototype.clone();

			// Usage of values in prototype to derive a new value.
			tempotype.setX(tempotype.getX() * i);
			tempotype.printX();
		}
	}

}
