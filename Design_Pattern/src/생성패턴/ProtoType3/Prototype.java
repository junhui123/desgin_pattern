package 생성패턴.ProtoType3;


//ProtoType Class
//http://leetaehoon.tistory.com/55
public abstract class Prototype implements Cloneable {
	public abstract void setX(int x);
	public abstract void printX();
	public abstract int getX();
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return (Prototype) super.clone();
	}
}
