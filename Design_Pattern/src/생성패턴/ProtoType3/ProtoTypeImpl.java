package 생성패턴.ProtoType3;

//concreateProtoType
//Implementation of prototype class
public class ProtoTypeImpl extends Prototype {

	int x;

	public ProtoTypeImpl(int x) {
		this.x = x;
	}

	@Override
	public void setX(int x) {
		this.x = x;
	}

	@Override
	public void printX() {
		System.out.println("value = " + x);
	}

	@Override
	public int getX() {
		return x;
	}

}
