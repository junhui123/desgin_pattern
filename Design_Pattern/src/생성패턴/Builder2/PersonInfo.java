package 생성패턴.Builder2;

public class PersonInfo {
	private String name;
	private int age;
	private String favColor;
	private String favAnimal;
	private int favNum;

	public PersonInfo(String name, Integer age, String favoriteColor, String favoriteAnimal, Integer favoriteNumber) {
		this.name = name;
		this.age = age;
		this.favColor = favoriteColor;
		this.favAnimal = favoriteAnimal;
		this.favNum = favoriteNumber;
	}
	
	//new PersonInfo("JDM", 20, null, null, null); => 가독성이 안좋음. 필드가 추가될 때 마다 null 늘어남
	//입력 순서가 달라지는 경우 favAnimal에 사람의 이름이 들어갈 수 있음.
	public PersonInfo(String name, Integer age){
	    this(name, age, null, null, null);
	}
	
	public PersonInfo(){
		
	}
	
	public static PersonInfoBuilder Builder(){
	    return new PersonInfoBuilder();
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public String getFavColor() {
		return favColor;
	}

	public String getFavAnimal() {
		return favAnimal;
	}

	public int getFavNum() {
		return favNum;
	}

	public String getPersonInfo() {
		String personInfo = String.format("name:%s, age:%d, favoriteColor:%s, favoriteAnimal:%s, favoriteNumber:%d", name, age, favColor, favAnimal, favNum);
		return personInfo;
	}

}
