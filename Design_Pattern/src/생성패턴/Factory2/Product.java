package 생성패턴.Factory2;

//생성할 객체 정의
public abstract class Product {
	public abstract String getName();
	public abstract int getPrice();
	
	@Override
	public String toString() {
		return "Product Name = " + getName() + ", Price = " + getPrice();
	}
}
