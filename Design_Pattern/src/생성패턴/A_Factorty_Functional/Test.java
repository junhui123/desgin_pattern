package 생성패턴.A_Factorty_Functional;

import java.util.function.Supplier;

public class Test {
  public static void main(String[] args) {
    
    Scientist alber = new Scientist( () -> new MyPenClass()   );
    alber.writeData("test");
    
    Supplier bicPen = () -> new BrandPen("Bic");
    
    Scientist thomas = new Scientist(bicPen);
//    Scientist nicola = new Scientist(BrandPen::bicPen);
    thomas.writeData("thomas");
    
    Scientist erwin = new Scientist(color -> new ColorPen(color, "Bic"));
    erwin.writeData("erwin");
    
  }
}
