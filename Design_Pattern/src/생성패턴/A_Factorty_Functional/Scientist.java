package 생성패턴.A_Factorty_Functional;

import java.awt.Color;
import java.util.function.Function;
import java.util.function.Supplier;

public class Scientist {
  private Pen pen;

  //pre java8
//  private PenFactory penerator;
//  public Scientist(PenFactory penerator) {
//    this.penerator = penerator;
//    this.pen = penerator.create();
//  }
  
  private Supplier<Pen> penerator;
  public Scientist(Supplier<Pen> penerator) {
    this.penerator = penerator;
    this.pen = penerator.get();
  }
  
  Function<Color, Pen> colorPen;
  public Scientist(Function<Color, Pen> colorPen) {
    this.colorPen = colorPen;
    this.pen = colorPen.apply(Color.RED);
  }
  

  public void writeData(String data) {
    if (pen.outOfInk()) {
      pen =  penerator.get();
    }
    pen.write(data);
  }
}
