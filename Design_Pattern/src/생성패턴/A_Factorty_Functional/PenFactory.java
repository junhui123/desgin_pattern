package 생성패턴.A_Factorty_Functional;

public interface PenFactory {
  Pen create();
}
