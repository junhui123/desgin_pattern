package 생성패턴.A_Factorty_Functional;

public class BrandPen implements Pen {

  private String str;
  
  public BrandPen(String string) {
    str = string;
  }

  @Override
  public void write(String toWrite) {
    System.out.println("BrandPen = "+ toWrite);
  }

  @Override
  public boolean outOfInk() {
    return false;
  }

}
