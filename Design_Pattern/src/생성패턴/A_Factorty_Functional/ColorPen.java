package 생성패턴.A_Factorty_Functional;

import java.awt.Color;

public class ColorPen implements Pen {

  private Color color = Color.white;
  private String str;
  
  public ColorPen(Color color, String string) {
    this.color = color;
    this.str = string;
  }

  @Override
  public void write(String toWrite) {
    System.out.println("ColorPen = " + toWrite);
    System.out.println("color = " + color);
    System.out.println("str = " + str);
  }

  @Override
  public boolean outOfInk() {
    // TODO Auto-generated method stub
    return false;
  }

}
