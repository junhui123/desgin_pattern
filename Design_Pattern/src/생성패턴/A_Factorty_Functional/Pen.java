package 생성패턴.A_Factorty_Functional;

public interface Pen {
  void write(String toWrite);
  boolean outOfInk();
}
