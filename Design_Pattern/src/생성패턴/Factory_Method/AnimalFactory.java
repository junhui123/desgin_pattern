package 생성패턴.Factory_Method;

//Product (Animal) 타입 객체 생성 메소드 선언
//concreate product 타입 객체를 반환
public class AnimalFactory {
    public static Animal create(String animalName){
        if (animalName == null) {
            throw new IllegalArgumentException("null은 안 되지롱~");
        }
        if (animalName.equals("소")) {
            return new Cow();
        }else if (animalName.equals("고양이")) {
            return new Cat();
        }else if (animalName.equals("개")) {
            return new Dog();
        }else{
            return null;
        }
    }
}

////인터페이스로 나누어저 creator / concreateCreatoe로 분할 가능
//interface Factory {
//	Animal create(String animalname);
//}

//public class AnimalFactoryTest implements Factory {
//	@Override
//	public Animal create(String animalname) {
//		if (animalname == null) {
//			throw new IllegalArgumentException("null은 안 되지롱~");
//		}
//		if (animalname.equals("소")) {
//			return new Cow();
//		} else if (animalname.equals("고양이")) {
//			return new Cat();
//		} else if (animalname.equals("개")) {
//			return new Dog();
//		} else {
//			return null;
//		}
//	}
//}