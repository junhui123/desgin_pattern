package 생성패턴.Factory_Method;

public class Test {
	public static void main(String[] args) {
		
		// Factory Method Pattern 
		// 정확히 어떤 클래스의 인스턴스인지 신경쓰지 않고 구현할 수 있는 장점
		// Animal의 구현체가 더 늘어나면 어떻게 될까요? 전부 new AnotherAnimal()과 같이 생성하는 것보다는 Facotry의 create()메쏘드만 수정
		
		Animal a1 = AnimalFactory.create("소");
		a1.printDescription();
		Animal a2 = AnimalFactory.create("고양이");
		a2.printDescription();
		Animal a3 = AnimalFactory.create("개");
		a3.printDescription();
	}
}
