package 생성패턴.Factory_Method;

//Product
//객체를 생성하는 인터페이스 정의
//생성은 상속받은 서브 클래스에서 생성
public interface Animal {
	public void printDescription();
}
