package 생성패턴.Builder4;

public class fooBuilder3 {
  private static String a;
  private static String b;
  private static String c;
  private static String d;
  private static String e;
  private static String f;
  private static String g;
  private static String h;
  private static String i;
  private static String j;
  private static String k;
  private static String l;

  private static fooBuilder3 fb3; 
  
  public static fooBuilder3 setA(String a) {
    fooBuilder3.a = a; 
    return fb3;
  }

  public static fooBuilder3  setB(String b) {
    fooBuilder3.b = b; return fb3;
  }

  public static fooBuilder3   setC(String c) {
    fooBuilder3.c = c; return fb3;
  }

  public static fooBuilder3   setD(String d) {
    fooBuilder3.d = d; return fb3;
  }

  public static fooBuilder3   setE(String e) {
    fooBuilder3.e = e; return fb3;
  }

  public static fooBuilder3   setF(String f) {
    fooBuilder3.f = f; return fb3;
  }

  public static fooBuilder3   setG(String g) {
    fooBuilder3.g = g; return fb3;
  }

  public static fooBuilder3   setH(String h) {
    fooBuilder3.h = h; return fb3;
  }

  public static fooBuilder3   setI(String i) {
    fooBuilder3.i = i; return fb3;
  }

  public static fooBuilder3   setJ(String j) {
    fooBuilder3.j = j; return fb3;
  }

  public static fooBuilder3   setK(String k) {
    fooBuilder3.k = k; return fb3;
  }

  public static fooBuilder3   setL(String l) {
    fooBuilder3.l = l; return fb3;
  }

  public static foo buildfoo3() {
    return new foo(a, b, c, d, e, f, g, h, i, j, k, l); 
  }
}
