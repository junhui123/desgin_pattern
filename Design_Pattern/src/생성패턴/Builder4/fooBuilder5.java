package 생성패턴.Builder4;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

//Supplier / Consumer 를 사용한 builder pattern
public class fooBuilder5<T> {

  // The blank final field _instance may not have been initialized
  // ==> private final Supplier<T> _instance = null;
  // 또는 생성자 or 메소드를 통한 초기화

  private Supplier<T> _instance = null;
  private List<Consumer<T>> _setters = new ArrayList<>();

  private fooBuilder5(Supplier<T> instance) {
    this._instance = instance;
  }

  public static <T> fooBuilder5<T> of(Supplier<T> instance) {
    return new fooBuilder5<T>(instance);
  }
  
  public <U> fooBuilder5<T> with(BiConsumer<T, U> consumer, U value) {
    Consumer<T> c = setter -> consumer.accept(setter, value);
    _setters.add(c);
    return this;
  }
  
  public T buildefoo5() {
    T value = _instance.get();
    _setters.forEach(iter -> iter.accept(value));
    _setters.clear();
    return value;
  }
}
