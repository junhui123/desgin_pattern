package 생성패턴.Builder4;

public class fooBuilder2 {
  private String a;
  private String b;
  private String c;
  private String d;
  private String e;
  private String f;
  private String g;
  private String h;
  private String i;
  private String j;
  private String k;
  private String l;
  
  public static class Build {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    
    public Build(){
      
    }

    public Build setA(String a) {
      this.a = a;
      return this;
    }

    public Build setB(String b) {
      this.b = b;
      return this;
    }

    public Build setC(String c) {
      this.c = c;
      return this;
    }

    public Build setD(String d) {
      this.d = d;
      return this;
    }

    public Build setE(String e) {
      this.e = e;
      return this;
    }

    public Build setF(String f) {
      this.f = f;
      return this;
    }

    public Build setG(String g) {
      this.g = g;
      return this;
    }

    public Build setH(String h) {
      this.h = h;
      return this;
    }

    public Build setI(String i) {
      this.i = i;
      return this;
    }

    public Build setJ(String j) {
      this.j = j;
      return this;
    }

    public Build setK(String k) {
      this.k = k;
      return this;
    }

    public Build setL(String l) {
      this.l = l;
      return this;
    }

    public foo buildfoo2() {
      return new foo(a, b, c, d, e, f, g, h, i, j, k, l); 
    }
  }
  
  public fooBuilder2(Build b) {
    this.a = b.a;
    this.b = b.b;
    this.c = b.c;
    this.d = b.d;
    this.e = b.e;
    this.f = b.f;
    this.g = b.g;
    this.h = b.h;
    this.i = b.i;
    this.j = b.j;
    this.k = b.k;
    this.l = b.l;
  }            

  
}
