package 생성패턴.Builder4;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

//Functional Interface Consumer 사용
public class fooBuilder4  {
  private String a;
  private String b;
  private String c;
  private String d;
  private String e;
  private String f;
  private String g;
  private String h;
  private String i;
  private String j;
  private String k;
  private String l;

  public fooBuilder4 (String a, String b, String c, String d, String e, String f, String g, String h, String i, String j, String k, String l) {
    super();
    this.a = a;
    this.b = b;
    this.c = c;
    this.d = d;
    this.e = e;
    this.f = f;
    this.g = g;
    this.h = h;
    this.i = i;
    this.j = j;
    this.k = k;
    this.l = l;
  }
  
  public fooBuilder4() {
    
  }

  public fooBuilder4  setA(String a) {
    this.a = a;
    return this;
  }

  public fooBuilder4  setB(String b) {
    this.b = b;
    return this;
  }

  public fooBuilder4  setC(String c) {
    this.c = c;
    return this;
  }

  public fooBuilder4  setD(String d) {
    this.d = d;
    return this;
  }

  public fooBuilder4  setE(String e) {
    this.e = e;
    return this;
  }

  public fooBuilder4  setF(String f) {
    this.f = f;
    return this;
  }

  public fooBuilder4  setG(String g) {
    this.g = g;
    return this;
  }

  public fooBuilder4  setH(String h) {
    this.h = h;
    return this;
  }

  public fooBuilder4  setI(String i) {
    this.i = i;
    return this;
  }

  public fooBuilder4  setJ(String j) {
    this.j = j;
    return this;
  }

  public fooBuilder4  setK(String k) {
    this.k = k;
    return this;
  }

  public fooBuilder4  setL(String l) {
    this.l = l;
    return this;
  }
  
  public static class Builder {
    private foo _myFoo;
    private List<Consumer<foo>> _setters = new ArrayList<>();
    public <T> Builder whit(BiConsumer<foo, T> setter, T val) {
      if(_myFoo == null) 
        _myFoo = new foo();
      Consumer<foo> c = _myFoo -> setter.accept(_myFoo, val);
      _setters.add(c);
      return this;
    }
    
    public foo buildfoo4() {
      foo val = _myFoo;
      _setters.forEach(_setters -> _setters.accept(val));
      _setters.clear();
      return _myFoo;
    }
  }
}
