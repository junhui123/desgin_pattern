package 생성패턴.Builder4;

public class fooBuilder {
  private String a;
  private String b;
  private String c;
  private String d;
  private String e;
  private String f;
  private String g;
  private String h;
  private String i;
  private String j;
  private String k;
  private String l;

  public fooBuilder setA(String a) {
    this.a = a;
    return this;
  }

  public fooBuilder setB(String b) {
    this.b = b;
    return this;
  }

  public fooBuilder setC(String c) {
    this.c = c;
    return this;
  }

  public fooBuilder setD(String d) {
    this.d = d;
    return this;
  }

  public fooBuilder setE(String e) {
    this.e = e;
    return this;
  }

  public fooBuilder setF(String f) {
    this.f = f;
    return this;
  }

  public fooBuilder setG(String g) {
    this.g = g;
    return this;
  }

  public fooBuilder setH(String h) {
    this.h = h;
    return this;
  }

  public fooBuilder setI(String i) {
    this.i = i;
    return this;
  }

  public fooBuilder setJ(String j) {
    this.j = j;
    return this;
  }

  public fooBuilder setK(String k) {
    this.k = k;
    return this;
  }

  public fooBuilder setL(String l) {
    this.l = l;
    return this;
  }

  public foo buildfoo1() {
    return new foo(a, b, c, d, e, f, g, h, i, j, k, l);
  }
}
