package 생성패턴.Builder4;

public class Test {
  public static void main(String[] args) {

    String a = "";
    String b = "";
    String c = "";
    String d = "";
    String e = "";
    String f = "";
    String g = "";
    String h = "";
    String i = "";
    String j = "";
    String k = "";
    String l = "";

    
    // 기존
    foo myFoo = null;
    myFoo = new foo(a, b, c, d, e, f, g, h, i, j, k, l);

    /*** 1. GoF ***/
    // 일반 builder 패턴
    // foo 생성을 위해 fooBuilder 생성 필요.
    fooBuilder myFooBuilder = new fooBuilder();
    myFoo = myFooBuilder.setA("1a").setB("1b").setC("1c").setD("1d").buildfoo1();
    System.out.println(myFoo.print());
    
    /*** 2. java static  ***/
    // static inner class
    // 1개의 foo 객체 생성을 위하여 반드시 1개의 fooBuilder 객체를 생성해야 하는 문제는 남아 있습니다.
    // 메모리를 효율적으로 사용해야 하는 환경에서는 적합하지 않습니다.
    // 또한 builder class가 inner class 로 포함되면서 클래스 재사용성에 대한 고민을 다시 해야 합니다.
    myFoo = new fooBuilder2.Build().setA("2a").setB("2b").setC("2c").setD("2d").buildfoo2();
    System.out.println(myFoo.print());
    
    //static class 
    //thread safe 하지 않음. foo 객체 멋대로 생성 가능..
    myFoo = fooBuilder3.setA("3a").setB("3b").setC("3c").setD("3d").buildfoo3();
    System.out.println(myFoo.print());
    
    /***
     * 3. java8 function interface default와 static 메소드는 functional interface 규약를
     * 깨지않고 선언된다.
     * 
     * @FunctionalInterface 
     * public interface FunctionalDefaultMethods { 
     *  void method(); 
     *  default void defaultMethod() {}
     * }
     * 
     * 장점
     *   1) 코드 재사용 : 생성할 Class 에 존재하는 setter 를 Builder Class에 중복으로 생성하지 않고 변수에 담아 재사용
     *   
     * 단점
     *  1)비슷한 Builder를 매번 재구현
     * 
     ****/
//    myFoo = new foo.fooBuilder().with(foo::setA, "4a").with(foo::setB, "4b").with(foo::setC, "4c").with(foo::setD, "4d").buildfoo();
      myFoo = new fooBuilder4.Builder().whit(foo::setA, "4a").whit(foo::setB, "4b").whit(foo::setC, "4c").whit(foo::setD, "4d").buildfoo4();
      System.out.println(myFoo.print());
    
    
    /**
     * 장점
     * 1) 코드 재사용 : GenericBuilder 클래스를 범용적으로 재사용 가능
     * 
     * 단점은
     *  1) inner class 를 사용할 때 보다 직관적이지 않다.
     */
      myFoo = fooBuilder5.of(foo::new).with(foo::setA, "5a").with(foo::setB, "5b").with(foo::setC, "5c").with(foo::setD, "5d").buildefoo5();
      System.out.println(myFoo.print());
    
  }
}
