package 생성패턴.Builder4;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class foo {
  private String a = "";
  private String b = "";
  private String c = "";
  private String d = "";
  private String e = "";
  private String f = "";
  private String g = "";
  private String h = "";
  private String i = "";
  private String j = "";
  private String k = "";
  private String l = "";

  // 생성자 가독성 떨어짐. 생성 복잡
  // 빌더 패턴 적용 new builder(().a(a).b(b)..build()로 변경 가독성 및 객체 생성 정확도 보장
  public foo(String a, String b, String c, String d, String e, String f, String g, String h, String i, String j, String k, String l) {
    this.a = a;
    this.b = b;
    this.c = c;
    this.d = d;
    this.e = e;
    this.f = f;
    this.g = g;
    this.h = h;
    this.i = i;
    this.j = j;
    this.k = k;
    this.l = l;
  }

  public foo() {

  }
  
  public foo setA(String a) {
    this.a = a;
    return this;
  }

  public foo setB(String b) {
    this.b = b;
    return this;
  }

  public foo setC(String c) {
    this.c = c;
    return this;
  }

  public foo setD(String d) {
    this.d = d;
    return this;
  }

  public foo setE(String e) {
    this.e = e;
    return this;
  }

  public foo setF(String f) {
    this.f = f;
    return this;
  }

  public foo setG(String g) {
    this.g = g;
    return this;
  }

  public foo setH(String h) {
    this.h = h;
    return this;
  }

  public foo setI(String i) {
    this.i = i;
    return this;
  }

  public foo setJ(String j) {
    this.j = j;
    return this;
  }

  public foo setK(String k) {
    this.k = k;
    return this;
  }

  public foo setL(String l) {
    this.l = l;
    return this;
  }

  public String print() {
    return a + ", " + b + ", " + c + ", " + d + ", " + e + ", " + f + ", " + g + ", " + h + ", " + i + ", " + j + ", " + k + ", " + l;
  }

  public static class fooBuilder {
    private foo _myFoo;
    private List<Consumer<foo>> _setters = new ArrayList<>();

    public <T> fooBuilder with(BiConsumer<foo, T> setter, T val) {
      if (_myFoo == null)
        _myFoo = new foo();
      Consumer<foo> c = _myFoo -> setter.accept(_myFoo, val);
      _setters.add(c);
      return this;
    }

    public foo buildfoo() {
      foo val = _myFoo;
      _setters.forEach(_setters -> _setters.accept(val));
      _setters.clear();
      return _myFoo;
    }
  }

}
