package 생성패턴.Singletone;

//내부 클래스가 호출되는 시점에 최초 생성이
//속도도 빠르고 필요치 않다면 생성하지도 않음.
public class Singleton4 {
	private Singleton4() {
	}

	private static class SingletonHolder {
		static final Singleton4 single = new Singleton4();
	}

	public static Singleton4 getInstatnce() {
		return SingletonHolder.single;
	}
}
