package 생성패턴.Singletone;

public class Test {
	public static void main(String[] args) {
		Test t = new Test();
		t.Amethod();
		t.Bmethod();
		
		Singleton1.getInstance().callTest();
		Singleton2.getInstance().callTest();
		
		
	}

	public void Amethod() {
		SingletonCounter sc = SingletonCounter.getInstance();
		System.out.println("Amethod에서 카운터 호출 " + sc.getNextInt());
	}

	public void Bmethod() {
		SingletonCounter sc = SingletonCounter.getInstance();
		System.out.println("Bmethod에서 카운터 호출 " + sc.getNextInt());
	}
}
