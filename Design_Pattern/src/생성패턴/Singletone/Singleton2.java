package 생성패턴.Singletone;

//getInstance가 호출 될 때 생성
//synchronized 키워드가 붙어 있어 성능 안좋음
//인스턴스를 사용할 필요가 없을때는 인스턴스 생성되지 않는다는 점이 장점
//멀티 스레드 환경에서 getInstance() 수행 시 하나의 스레드는 null을 확인
//다른 스레드로 제어권이 넘어가 null임을 만족 시 인스턴스가 여러개 생성 가능
public class Singleton2 {
    private static Singleton2 single;
    
    //매번 동기화를 수행하므로 자바 메모리 모델에 따른 각종 문제를 방지할 수 있어 thread-safe합니다. 
    //단점은 매번동기화를 수행하므로 수행 비용이 높다
    //getInstance 메소드를 synchronized 사용하여 동기화 
    //성능 저하 되지만 인스턴스 중복 생성 방지
    public static synchronized Singleton2 getInstance(){
        if (single == null) {
            single = new Singleton2();
        }
        return single;
    }
    
    private Singleton2(){
    	System.out.println("call private singleton2");
    }
    
	public void callTest() {
		System.out.println("Call Test singleton2");
	}
}
