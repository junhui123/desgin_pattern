package 생성패턴.Singletone;

// 싱글톤 구현 방법 
// 클래스가 JVM에 로딩 될 때 JVM에서 유일한 인스턴스 생성
// 장점은 동기화부담이 적다는 것입니다. 단 한번 클래스로더가
// thread-safe하게 클래스를 로드하는 시점에 클래스가 로딩되어 안전합니다.
// 단점은 인스턴스가 미리부터 생성된다는 것입니다. 

public class Singleton1 {
	private static Singleton1 single = new Singleton1();

	public static Singleton1 getInstance() {
		return single;
	}

	private Singleton1() {
		System.out.println("call private singleton1");
	}
	
	public void callTest() {
		System.out.println("Call Test singleton1");
	}
}
