package 생성패턴.Singletone;


//volatile 키워드를 사용하면 자바의 일종의 최적화인 리오더링
//(보통 컴파일 과정에서 일어나며, 프로그래머가 만들어낸 코드는 컴파일 될 때
//좀더 빠르게 실행될 수 있도록 조작이 가해져 최적하됨)을 회피하여 읽기와 쓰기순서를 보장
//멀티스레딩을 쓰더라도 single변수가 Singleton 인스턴스로 초기화 되는 과정이 올바르게 진행되도록
//할 수 있다.
public class Singleton3 {
	//volatile 변수의 원자성 보장 (JVM 최소 실행 단위)
	private volatile static Singleton3 single;

	public static Singleton3 getInstance() {
		if (single == null) {
			synchronized (Singleton3.class) {
				if (single == null) {
					single = new Singleton3();
				}
			}
		}
		return single;
	}

	private Singleton3() {
	}
}
