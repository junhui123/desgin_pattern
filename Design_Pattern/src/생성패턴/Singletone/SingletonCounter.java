package 생성패턴.Singletone;

/**
 * 유일한 인스턴스 생성에 사용
 * 자주 사용되는 객체는 한번만 생성하고 Heap에 존재하는 이 객체를 가르키도록만 만들면 된다.
 * 즉, 객체가 생성될 때 Heap 영역에 올라가는 시간과 메모리를 줄일 수 있다.
 * 
 * http://suein1209.tistory.com/397
 * http://iilii.egloos.com/3807664
 */

public class SingletonCounter {
	//자기자신의 클래스 인스턴스를 private로 가짐
	private static SingletonCounter singleton = new SingletonCounter();
	private int cnt = 0;
	
	//외부에서 클래스 호출 시 인스턴스 생성하지 못하도록 private 생성자 가짐
	private SingletonCounter() {
	}

	//getInstance를 통해 static 객체를 호출
	//getInstance()는 보통 싱글톤 패턴에서 쓰임
	//예외로 Calendar.getInstance() == Factory Method 패턴...
	public static SingletonCounter getInstance() {
		return singleton;
	}

	public int getNextInt() {
		return ++cnt;
	}
}
