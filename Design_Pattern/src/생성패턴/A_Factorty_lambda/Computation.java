package 생성패턴.A_Factorty_lambda;

@FunctionalInterface
public interface Computation {
  public long sum1To(long n);
}
