package 생성패턴.A_Factorty_lambda;

import java.util.function.Supplier;

public class ComputationFactoryClient {
  public static void main(String... a) {
    final Supplier<Computation> slowFactory  = () -> n -> {
      long rv = 0L;
      for(long i = 1; i <= n ; ++i) {
        rv += i;
      }
      return rv;
    };
    
    final Supplier<Computation> gasusFactoryt = () -> n -> (n * (n+1))/2;
    System.out.println(slowFactory.get().sum1To(100));
    System.out.println(gasusFactoryt.get().sum1To(100));
  }
}
