package 생성패턴.AbstractFactory;

//추상 팩토리 클래스 패턴은 인터페이스 보다는 구조체에 접근할 수 있는 코드를 제공한다.
//추상 팩토리 클래스 패턴은 확장에 매우 용의한 패턴으로 쉽게 다른 서브 클래스들을 확장할 수 있다.
//추상 팩토리 클래스 패턴은 기존 팩토리 패턴의 if-else 로직에서 벗어날 수 있게 해준다.
public class Test {
	public static void main(String[] args) {
		Product com = ProductFactory.getProduct(new ComputerFactory("com1", 2000));
		Product tk = ProductFactory.getProduct(new TicketFactory("tk1", 12000));

		System.out.println(com.toString());
		System.out.println(tk.toString());
	}
}
