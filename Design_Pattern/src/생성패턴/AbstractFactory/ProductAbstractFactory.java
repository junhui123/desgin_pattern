package 생성패턴.AbstractFactory;


//Abstract Factory : 개념적 제품에 대한 객체 생성, 인터페이스로 정의
public interface ProductAbstractFactory {
	//Product 인스턴스 반환
	public Product createProduct();
}