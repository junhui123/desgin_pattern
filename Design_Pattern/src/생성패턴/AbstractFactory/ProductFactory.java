package 생성패턴.AbstractFactory;

public class ProductFactory {
	public static Product getProduct(ProductAbstractFactory product) {
		return product.createProduct();
	}
}
