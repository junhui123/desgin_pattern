package 생성패턴.AbstractFactory;

//Abstract Product : 객체 개념적 정의 
public abstract class Product {
	public abstract String getName();
	public abstract int getPrice();
	
	@Override
	public String toString() {
		return "Product Name = " + getName() + ", Price = " + getPrice();
	}
}
