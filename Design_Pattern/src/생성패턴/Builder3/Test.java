package 생성패턴.Builder3;

public class Test {
	public static void main(String[] args) {
		NutritionFacts cocaCola = new NutritionFacts.Builder(240, 8).calories(100).sodium(35).carbohydrate(27).build();
		cocaCola.print();
	}
}
